<?php

namespace Intellihot\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Intellihot\AppBundle\Entity\Submission;
use Intellihot\AppBundle\Form\SubmissionType;
use Intellihot\AppBundle\Entity\Option;

class ApiController extends FOSRestController
{
    /**
     * @Rest\View
     * @Rest\Get("businesses")
     */
    public function getBusinessesAction()
    {
        $businesses = $this->getDoctrine()->getManager()
            ->getRepository('IntellihotAppBundle:Business')
            ->findBy(array(), array('name' => 'ASC'));

        return $businesses;
    }

    /**
     * @Rest\View
     * @Rest\Get("fixtures")
     */
    public function getFixturesAction()
    {
        $fixtures = $this->getDoctrine()->getManager()
            ->getRepository('IntellihotAppBundle:Fixture')
            ->findBy(array(), array('business' => 'ASC', 'name' => 'ASC'));

        return $fixtures;
    }

    /**
     * @Rest\View
     * @Rest\Get("states")
     */
    public function getStatesAction()
    {
        $states = array();

        foreach (SubmissionType::$states as $code => $name) {
            $states[] = array(
                'code' => $code,
                'name' => $name
            );
        }

        return $states;
    }

    /**
     * @Rest\View
     * @Rest\Post("submission-preview")
     */
    public function postSubmissionPreviewAction(Request $request)
    {
        // Create form
        $submission = new Submission();
        $form       = $this->createForm(new SubmissionType(true, true), $submission);

        // format input
        $request->request->set('mountingOption', ucfirst($request->get('mountingOption')));

        // Handle form
        $form->handleRequest($request);

        // Process form
        if ($form->isValid()) {
            // Get fixture amount
            $fixtures = json_decode($form->get('fixtures')->getData(), true);
            $fixtures = $fixtures == null ? array() : $fixtures;

            // Calculate fixture unit and fixture detail
            $calculatedFixtureUnit   = 0;
            $calculatedFixtureDetail = '';
            $fixtureObjects          = array();

            if (is_array($fixtures) && count($fixtures) > 0) {
                $fixtureObjects = $this->getDoctrine()->getManager()->getRepository('IntellihotAppBundle:Fixture')
                    ->findBy(array('id' => array_keys($fixtures)));

                foreach ($fixtureObjects as $fo) {
                    $calculatedFixtureUnit += $fixtures[$fo->getId()] * $fo->getValue();
                    $calculatedFixtureDetail .= " " . $fo->getName() . ": " . $fixtures[$fo->getId()] . " * " . $fo->getValue() . " = " . $fixtures[$fo->getId()] * $fo->getValue() . " fu \n";;
                }
            }

            if (count($fixtureObjects) == 0) {
                if ($form->get('fixtures')->getData() == null || strlen($form->get('fixtures')->getData()) < 1) {
                    return array(
                        'code'    => 400,
                        'message' => 'Form fields are not valid',
                        'errors'  => array(
                            'fixtures' => 'This value should not be blank.'
                        )
                    );
                } else {
                    return array(
                        'code'    => 400,
                        'message' => 'Form fields are not valid',
                        'errors'  => array(
                            'fixtures' => $form->get('fixtures')->getData() . ' is not valid'
                        )
                    );
                }
            }

            $submission->setFixtureDetails($calculatedFixtureDetail);
            $submission->setFixtureUnits($calculatedFixtureUnit);

            // Get project details
            $projectDetails = array(
                'businessId'      => $submission->getBusiness()->getId(),
                'businessName'    => $submission->getBusiness()->getName(),
                'rooms'           => $submission->getRooms(),
                'fixtureDetails'  => $submission->getFixtureDetails(),
                'asmeRequirement' => $submission->getAsmeRequirement(),
                'mountingOption'  => $submission->getMountingOption(),
                'fuelSupply'      => $submission->getFuelSupply(),
                'altitude'        => $submission->getAltitude(),
                'altitudeFactor'  => $submission->getAltitudeFactor(),
                'waterTempIn'     => $submission->getIncomingWaterTemperature(),
                'waterTempOut'    => $submission->getDesiredOutputTemperature(),
                'fixtureUnits'    => $submission->getFixtureUnits()
            );

            // convert fixture details from string to array
            $projectDetails['fixtureDetails'] = explode("\n", $projectDetails['fixtureDetails']);

            foreach ($projectDetails['fixtureDetails'] as $idx => $fd) {
                if (strlen(trim($fd)) < 1) {
                    unset($projectDetails['fixtureDetails'][$idx]);
                } else {
                    $projectDetails['fixtureDetails'][$idx] = trim($fd);
                }
            }

            // Calculate gpm, btu, altitudeFactor, selections
            $calculatedResult = $this->get('intellihot_app.calculator')->calculate(
                $submission->getBusiness()->getId(),
                $submission->getAsmeRequirement(),
                $submission->getMountingOption(),
                $submission->getAltitude(),
                $submission->getIncomingWaterTemperature(),
                $submission->getDesiredOutputTemperature(),
                $submission->getFixtureUnits()
            );

            if (isset($calculatedResult['gpm'])) {
                $submission->setGpm($calculatedResult['gpm']);
                $projectDetails['gpm'] = $submission->getGpm();
            }

            if (isset($calculatedResult['btu'])) {
                $submission->setBtu($calculatedResult['btu']);
                $projectDetails['btu'] = $submission->getBtu();
            }

            if (isset($calculatedResult['selections'])) {
                $submission->setSelections($calculatedResult['selections']);
                $projectDetails['selections'] = $submission->getSelections();
            }

            if (isset($calculatedResult['altitudeFactor'])) {
                $submission->setAltitudeFactor($calculatedResult['altitudeFactor']);
            }

            if ($calculatedResult['success'] == false) {
                return array(
                    'code'        => 200,
                    'project'     => $projectDetails,
                    'selections'  => array(),
                    'unsupported' => array(
                        'message' => $calculatedResult['msg_text'],
                        'contact' => $calculatedResult['contact']
                    )
                );
            }

            // Get selection details
            $selectionDetails = $this->get('intellihot_app.calculator')
                ->getSelectionDetails($projectDetails, $submission->getSelections());

            unset($projectDetails['selections']);

            foreach ($selectionDetails as $sd) {
                if ($sd['type'] == 'nomatch') {
                    if ($sd['name'] == "Move to Wall Mount") {
                        $errorMessage = 'Please try specifying wall-mounted units for your low-BTU project or contact our support team to discuss the needs of your project. Thank you.';
                    } else if ($sd['name'] == "Move to Floor Mount") {
                        $errorMessage = 'Please try specifying floor-mounted units for your high-BTU project or contact our support team to discuss the needs of your project. Thank you.';
                    } else if ($sd['name'] == "Contact Support") {
                        $errorMessage = 'Sorry, the sizing calculator was unable to make a recommendation. Please contact our support team to discuss the needs of your project. Thank you.';
                    } else {
                        $errorMessage = 'Sorry, the sizing calculator was unable to make a recommendation. Please contact our support team to discuss the needs of your project. Thank you.';
                    }

                    $contactEmail = $this->getDoctrine()->getManager()
                        ->getRepository('IntellihotAppBundle:Option')
                        ->findOneBy(array('name' => Option::INTELLIHOT_EMAIL));

                    if ($contactEmail instanceof Option) {
                        $contactEmail = $contactEmail->getValue();
                    } else {
                        $contactEmail = null;
                    }

                    return array(
                        'code'        => 200,
                        'project'     => $projectDetails,
                        'selections'  => array(),
                        'unsupported' => array(
                            'message' => $errorMessage,
                            'contact' => $contactEmail
                        )
                    );
                }
            }

            // add image for selection unit
            foreach ($selectionDetails as $idx => $sd) {
                $selectionDetails[$idx]['image'] = $request->getUriForPath(
                    $this->get('templating.helper.assets')->getUrl('bundles/intellihotapp/images/heater-' . $sd['image'] . '.png')
                );
            }

            return array(
                'code'       => 200,
                'project'    => $projectDetails,
                'selections' => $selectionDetails
            );
        } else {
            $errors = array();

            foreach ($form->all() as $key => $child) {
                foreach ($child->getErrors(true) as $error) {
                    $errors[$key][] = $error->getMessage();
                }
            }

            foreach ($form->getErrors() as $key => $child) {
                if (count($child->getMessageParameters()) > 0) {
                    $errors[array_values($child->getMessageParameters())[0]] = $child->getMessage();
                }
            }

            return array(
                'code'    => 400,
                'message' => 'Form fields are not valid',
                'errors'  => $errors
            );
        }
    }

    /**
     * @Rest\View
     * @Rest\Post("submission")
     */
    public function postSubmissionAction(Request $request)
    {
        // Create form
        $submission = new Submission();
        $form       = $this->createForm(new SubmissionType(true), $submission);

        // format input
        $request->request->set('mountingOption', ucfirst($request->get('mountingOption')));

        // Handle form
        $form->handleRequest($request);

        // Process form
        if ($form->isValid()) {
            // Get fixture amount
            $fixtures = json_decode($form->get('fixtures')->getData(), true);
            $fixtures = $fixtures == null ? array() : $fixtures;

            // Calculate fixture unit and fixture detail
            $calculatedFixtureUnit   = 0;
            $calculatedFixtureDetail = '';
            $fixtureObjects          = array();

            if (is_array($fixtures) && count($fixtures) > 0) {
                $fixtureObjects = $this->getDoctrine()->getManager()->getRepository('IntellihotAppBundle:Fixture')
                    ->findBy(array('id' => array_keys($fixtures)));

                foreach ($fixtureObjects as $fo) {
                    $calculatedFixtureUnit += $fixtures[$fo->getId()] * $fo->getValue();
                    $calculatedFixtureDetail .= " " . $fo->getName() . ": " . $fixtures[$fo->getId()] . " * " . $fo->getValue() . " = " . $fixtures[$fo->getId()] * $fo->getValue() . " fu \n";;
                }
            }

            if (count($fixtureObjects) == 0) {
                if ($form->get('fixtures')->getData() == null || strlen($form->get('fixtures')->getData()) < 1) {
                    return array(
                        'code'    => 400,
                        'message' => 'Form fields are not valid',
                        'errors'  => array(
                            'fixtures' => 'This value should not be blank.'
                        )
                    );
                } else {
                    return array(
                        'code'    => 400,
                        'message' => 'Form fields are not valid',
                        'errors'  => array(
                            'fixtures' => $form->get('fixtures')->getData() . ' is not valid'
                        )
                    );
                }
            }

            $submission->setFixtureDetails($calculatedFixtureDetail);
            $submission->setFixtureUnits($calculatedFixtureUnit);

            // Calculate gpm, btu, altitudeFactor, selections
            $calculatedResult = $this->get('intellihot_app.calculator')->calculate(
                $submission->getBusiness()->getId(),
                $submission->getAsmeRequirement(),
                $submission->getMountingOption(),
                $submission->getAltitude(),
                $submission->getIncomingWaterTemperature(),
                $submission->getDesiredOutputTemperature(),
                $submission->getFixtureUnits()
            );

            if ($calculatedResult['success']) {
                $submission
                    ->setGpm($calculatedResult['gpm'])
                    ->setBtu($calculatedResult['btu'])
                    ->setAltitudeFactor($calculatedResult['altitudeFactor'])
                    ->setSelections($calculatedResult['selections']);
            } else {
                return array(
                    'code'    => 400,
                    'message' => $calculatedResult['msg_text']
                );
            }

            // Get project details
            $projectDetails = array(
                'name'               => $submission->getName(),
                'email'              => $submission->getEmail(),
                'phone'              => $submission->getPhone(),
                'zipcode'            => $submission->getZipcode(),
                'projectName'        => $submission->getProjectName(),
                'projectNumber'      => $submission->getProjectNumber(),
                'projectDescription' => $submission->getProjectDescription(),
                'additionalComments' => $submission->getAdditionalComments(),
                'engineer'           => $submission->getEngineer(),
                'contractor'         => $submission->getContractor(),
                'businessId'         => $submission->getBusiness()->getId(),
                'businessName'       => $submission->getBusiness()->getName(),
                'rooms'              => $submission->getRooms(),
                'fixtureDetails'     => $submission->getFixtureDetails(),
                'asmeRequirement'    => $submission->getAsmeRequirement(),
                'mountingOption'     => $submission->getMountingOption(),
                'fuelSupply'         => $submission->getFuelSupply(),
                'altitude'           => $submission->getAltitude(),
                'altitudeFactor'     => $submission->getAltitudeFactor(),
                'waterTempIn'        => $submission->getIncomingWaterTemperature(),
                'waterTempOut'       => $submission->getDesiredOutputTemperature(),
                'fixtureUnits'       => $submission->getFixtureUnits(),
                'gpm'                => $submission->getGpm(),
                'btu'                => $submission->getBtu(),
                'selections'         => $submission->getSelections()
            );

            // Assign Rsm and Representatives to submission
            $this->get('intellihot_app.calculator')->assignRsmAndRepresentativeToSubmission($submission);

            // Get selection details
            $selectionDetails = $this->get('intellihot_app.calculator')
                ->getSelectionDetails($projectDetails, $submission->getSelections());

            foreach ($selectionDetails as $sd) {
                if ($sd['type'] == 'nomatch') {
                    if ($sd['name'] == "Move to Wall Mount") {
                        $errorMessage = 'Please try specifying wall-mounted units for your low-BTU project or contact our support team to discuss the needs of your project. Thank you.';
                    } else if ($sd['name'] == "Move to Floor Mount") {
                        $errorMessage = 'Please try specifying floor-mounted units for your high-BTU project or contact our support team to discuss the needs of your project. Thank you.';
                    } else if ($sd['name'] == "Contact Support") {
                        $errorMessage = 'Sorry, the sizing calculator was unable to make a recommendation. Please contact our support team to discuss the needs of your project. Thank you.';
                    } else {
                        $errorMessage = 'Sorry, the sizing calculator was unable to make a recommendation. Please contact our support team to discuss the needs of your project. Thank you.';
                    }

                    return array(
                        'code'    => 400,
                        'message' => $errorMessage
                    );
                }
            }

            // Generate pdfs
            $this->get('intellihot_app.calculator')->generatePdfs($submission, $fixtures, $selectionDetails);

            return array(
                'code'       => 200,
                'message'    => 'Your submission is submitted successfully',
                'submission' => $submission->getId(),
                'schematics' => explode("\n", $submission->getSchematics())
            );
        } else {
            $errors = array();

            foreach ($form->all() as $key => $child) {
                foreach ($child->getErrors(true) as $error) {
                    $errors[$key][] = $error->getMessage();
                }
            }

            foreach ($form->getErrors() as $key => $child) {
                if (count($child->getMessageParameters()) > 0) {
                    $errors[array_values($child->getMessageParameters())[0]] = $child->getMessage();
                }
            }

            return array(
                'code'    => 400,
                'message' => 'Form fields are not valid',
                'errors'  => $errors
            );
        }
    }
}
