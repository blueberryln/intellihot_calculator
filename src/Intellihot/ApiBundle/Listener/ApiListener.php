<?php

namespace Intellihot\ApiBundle\Listener;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Doctrine\ORM\EntityManager;

class ApiListener implements EventSubscriberInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Request Listener
     *
     * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
     * @throws AccessDeniedHttpException
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $uri = explode('/', $event->getRequest()->getPathInfo());

        if ($uri[1] == 'api') {
            $key = isset($uri[2]) ? $uri[2] : '';

            if (strlen($key) < 1) {
                throw new AccessDeniedHttpException('Missing token');
            } else {
                $client = $this->em
                    ->getRepository('IntellihotApiBundle:Client')
                    ->findOneBy(array('key' => $key));

                if ($client == null) {
                    throw new AccessDeniedHttpException('Invalid token');
                }
            }
        }
    }

    /**
     * Exception Listener
     *
     * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $uri = explode('/', $event->getRequest()->getPathInfo());

        if ($uri[1] == 'api') {
            $exception = $event->getException();

            $message = array(
                'code'    => method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : $exception->getCode(),
                'message' => $exception->getMessage()
            );

            // Customize your response object to display the exception details
            $response = new JsonResponse($message);

            // HttpExceptionInterface is a special type of exception that
            // holds status code and header details
            if ($exception instanceof HttpExceptionInterface) {
                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());
            } else {
                $response->setStatusCode(500);
            }

            // Send the modified response object to the event
            $event->setResponse($response);
        }
    }

    /**
     * Register Listener
     *
     * @return type
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST   => array(array('onKernelRequest', 9999)),
            KernelEvents::EXCEPTION => array(array('onKernelException', 100))
        );
    }
}