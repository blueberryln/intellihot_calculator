<?php

namespace Intellihot\AppBundle\Service;

use Doctrine\ORM\EntityManager;

use Intellihot\AppBundle\Entity\Submission;
use Intellihot\AppBundle\Entity\Rsm;
use Intellihot\AppBundle\Entity\Representative;
use Intellihot\AppBundle\Entity\Option;

class Mailer
{
    private $em;
    private $twig;
    private $mailer;
    private $parameters;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, EntityManager $em)
    {
        $this->mailer = $mailer;
        $this->twig   = $twig;
        $this->em     = $em;
    }

    public function sendSuccessEmail(Submission $submission, $selectionDetails)
    {
        // Get options
        $options = $this->em->getRepository('IntellihotAppBundle:Option')->findBy(array(
            'name' => array(
                Option::MANAGER_EMAIL_ROUTING,
                Option::REPRESENTATIVE_EMAIL_ROUTING,
                Option::INTELLIHOT_EMAIL_ROUTING,
                Option::INTELLIHOT_EMAIL,
                Option::INTELLIHOT_EMAIL_NAME
            )
        ));

        $rsmEmailRouting            = null;
        $representativeEmailRouting = null;
        $intellihotEmailRouting     = null;

        foreach ($options as $op) {
            if ($op->getName() == Option::MANAGER_EMAIL_ROUTING) {
                $rsmEmailRouting = $op;
            } else if ($op->getName() == Option::REPRESENTATIVE_EMAIL_ROUTING) {
                $representativeEmailRouting = $op;
            } else if ($op->getName() == Option::INTELLIHOT_EMAIL_ROUTING) {
                $intellihotEmailRouting = $op;
            } else if ($op->getName() == Option::INTELLIHOT_EMAIL) {
                $intellihotEmail = $op;
            } else if ($op->getName() == Option::INTELLIHOT_EMAIL_NAME) {
                $intellihotEmailName = $op;
            }
        }

        $templateName = 'IntellihotAppBundle:Calculator:success_email.txt.twig';

        $context = array(
            'submission' => $submission,
            'selections' => $selectionDetails
        );

        // send mail to user
        $this->sendMessage(
            $templateName,
            $context,
            $intellihotEmail->getValue(),
            $intellihotEmailName->getValue(),
            $submission->getEmail()
        );

        // send mail to regional sales and representatives
        $bccEmails = array();

        // BCC admin
        if ($intellihotEmailRouting instanceof Option && $intellihotEmailRouting->getValue() == Option::INTELLIHOT_EMAIL_ROUTING_ON) {
            if ($intellihotEmail->getValue() && $intellihotEmailName->getValue()) {
                $bccEmails[$intellihotEmail->getValue()] = $intellihotEmailName->getValue();
            }
        }

        // BCC regional sales manager
        if ($rsmEmailRouting instanceof Option && $rsmEmailRouting->getValue() == Option::MANAGER_EMAIL_ROUTING_ON) {
            if ($submission->getRsm() instanceof Rsm) {
                $bccEmails[$submission->getRsm()->getEmail()] = $submission->getRsm()->getName();
            }
        }

        // BCC representatives
        if ($representativeEmailRouting instanceof Option && $representativeEmailRouting->getValue() == Option::REPRESENTATIVE_EMAIL_ROUTING_ON) {
            if (sizeof($submission->getRepresentatives()) > 0) {
                foreach ($submission->getRepresentatives() as $rep) {
                    if (($rep->getEmail() != null) && ($rep->getEmail() != 'n/a')) {
                        $bccEmails[$rep->getEmail()] = $rep->getCompany();
                    }
                }
            }
        }

        // BBC account managers of business type
        foreach (explode(';', $submission->getBusiness()->getNotificationEmails()) as $ne) {
            $tne = trim($ne);

            if (strlen($tne) > 0) {
                $bccEmails[$tne] = $tne;
            }
        }

        $context['include_user_information'] = true;

        $this->sendMessage(
            $templateName,
            $context,
            $intellihotEmail->getValue(),
            $intellihotEmailName->getValue(),
            $bccEmails
        );
    }

    /**
     * @param $templateName
     * @param $context
     * @param $fromEmail
     * @param $fromName
     * @param $toEmail
     * @param null $bccEmails
     */
    protected function sendMessage($templateName, $context, $fromEmail, $fromName, $toEmail, $bccEmails = null)
    {
        if (strlen(trim($fromEmail)) < 1) {
            return;
        }

        $template = $this->twig->loadTemplate($templateName);
        $subject  = $template->renderBlock('subject', $context);
        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromEmail, $fromName)
            ->setTo($toEmail);

        if ($bccEmails !== null) {
            $message->setBcc($bccEmails);
        }

        if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        $this->mailer->send($message);
    }
}