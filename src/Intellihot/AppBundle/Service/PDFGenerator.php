<?php

namespace Intellihot\AppBundle\Service;

use Intellihot\AppBundle\Entity\Submission;
use Exception;

class PDFGenerator
{
    private $parameters;

    public function __construct(array $parameters = array())
    {
        $this->parameters = $parameters;
    }

    /**
     * Generate Schematic Details
     *
     * @param PDF $pdf
     * @param Submission $submission
     * @param $selectedSchematic
     * @param $submissionFixtures
     * @return bool
     */
    private function generateSchematicDetails(PDF $pdf, Submission $submission, $selectedSchematic, $submissionFixtures)
    {
        try {
            $now = new \DateTime();

            $pdf->AddPage('P');
            $pdf->SetMargins(15, 15, 23);

            // Logo
            $pdf->Image(__DIR__ . "/../Resources/public/images/schematic-logo-updated.png", 15, 8, 50);
            $pdf->Ln(20);

            // Details
            $pdf->SetFont('Arial', '', 10);

            $pdf->Cell(0, 5, 'Galesburg, IL', 0);
            $pdf->Ln(5);
            $pdf->Cell(0, 5, $now->format('m/d/Y'), 0);
            $pdf->Ln(8);

            $pdf->Cell(0, 5, 'To:', 0);
            $pdf->Ln(5);
            $pdf->Cell(0, 5, $submission->getName(), 0);
            $pdf->Ln(8);
            $pdf->Cell(0, 5, 'Subject: ' . $submission->getProjectName() . ', ' . $submission->getCity() . ', ' . $submission->getState(), 0);
            $pdf->Ln(10);

            // Letter
            $firstAndLastName = explode(' ', $submission->getName());
            $pdf->Cell(0, 5, 'Dear ' . $firstAndLastName[0] . ',', 0);
            $pdf->Ln(7);
            $pdf->MultiCell(0, 5, 'Thank you for considering Intellihot to be your source for unlimited and efficient hot water. We have reengineered these units from ground up (backed by 28 patents) to outperform and outlast competitive products.', 0);
            $pdf->Ln(3);
            $pdf->MultiCell(0, 5, 'For your project, ' . $submission->getProjectName() . ', the following parameters were received as the inputs for this design:', 0);

            $pdf->Ln(3);
            $pdf->SetLeftMargin(23);
            $pdf->Cell(0, 5, '1. Application Type: ' . $submission->getBusiness()->getName(), 0);
            $pdf->Ln(5);
            $pdf->Cell(0, 5, '2. Inlet Temperature: ' . $submission->getIncomingWaterTemperature() . iconv("UTF-8", "ISO-8859-1", "°") . 'F', 0);
            $pdf->Ln(5);
            $pdf->Cell(0, 5, '3. Fixture Temp: ' . $submission->getDesiredOutputTemperature() . iconv("UTF-8", "ISO-8859-1", "°") . 'F', 0);
            $pdf->Ln(5);
            $pdf->Cell(0, 5, '4. Unit Setpoint: ' . $submission->getPointTemperature() . iconv("UTF-8", "ISO-8859-1", "°") . 'F', 0);
            $pdf->Ln(5);

            $nextItem = 5;
            if ($submission->getAltitude() > 0) {
                $pdf->Cell(0, 5, $nextItem . '. Altitude: ' . $submission->getAltitude(), 0);
                $pdf->Ln(5);

                $nextItem++;
            }

            if ($submission->getRooms() > 0) {
                $pdf->Cell(0, 5, $nextItem . '. Rooms: ' . $submission->getRooms(), 0);
                $pdf->Ln(5);

                $nextItem++;
            }

            foreach ($submissionFixtures as $f) {
                $pdf->Cell(0, 5, $nextItem . '. ' . $f, 0);
                $pdf->Ln(5);
                $nextItem++;
            }

            $pdf->SetLeftMargin(15);
            $pdf->Ln(3);
            $pdf->MultiCell(0, 5, 'Based on the above information we calculate this project has '
                . $submission->getFixtureUnits() . ' fixture units, with an estimated peak flow rate of '
                . number_format($submission->getGpm(), 0) . ' GPM, with a design load rate of '
                . number_format($submission->getBtu(), 0) . ' BTU/h.', 0);

            $pdf->Ln(3);
            $pdf->MultiCell(0, 5, 'Based on your model selection of ' . trim($selectedSchematic['name']) . ', we propose the following:', 0);

            $pdf->Ln(3);
            $pdf->SetLeftMargin(23);
            $pdf->Cell(0, 5, '1. Model: ' . trim($selectedSchematic['name']), 0);
            $pdf->Ln(5);
            $pdf->Cell(0, 5, '2. Number of units needed: ' . $selectedSchematic['quantity'], 0);
            $pdf->Ln(5);
            $pdf->Cell(0, 5, '3. Total input rate: ' . number_format($selectedSchematic['btu'], 0) . ' BTU/h', 0);


            $pdf->SetLeftMargin(15);
            $pdf->Ln(8);
            $pdf->MultiCell(0, 5, 'Thank you for the opportunity to serve you. If you have any questions, please do not hesitate to call or email me.', 0);

            $pdf->Ln(2);
            $pdf->Cell(0, 5, 'Sincerely,', 0);
            $pdf->Ln(8);

            $pdf->Image(__DIR__ . "/../Resources/public/images/imgo.jpg", 20, $pdf->GetY() - 2, 35);

            $pdf->Ln(15);
            $pdf->Cell(0, 5, 'Intellihot Applications Team', 0);
            //$pdf->Ln(5);
            //$pdf->Cell(0, 5, 'Application Engineer', 0);
            $pdf->Ln(5);
            $pdf->Cell(0, 5, 'applications@intellihot.com', 0);
            $pdf->Ln(5);
            $pdf->Cell(0, 5, 'Tel: 877.835.1705', 0);
            $pdf->Ln(10);

            $pdf->Cell(0, 5, 'Enclosures:', 0);
            $pdf->Ln(8);
            $pdf->Cell(0, 5, '1. Suggested Piping Diagram', 0);
            $pdf->Ln(5);
            $pdf->Cell(0, 5, '2. Model ' . trim(preg_replace('/[(]\S*[)]/', '', $selectedSchematic['name'])) . ' Submittal', 0);

            // Footer
            $pdf->SetLeftMargin(25);
            $pdf->Ln(35);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->SetY(270);
            $pdf->SetTextColor(151, 13, 36);
            $pdf->Cell(33, 5, 'www.intellihot.com', 0, '', '', false, 'http://www.intellihot.com');
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(66, 5, iconv("UTF-8", "cp1252", "• ") . '2900 W. Main St., Galesburg, IL 61401', 0);
            $pdf->Cell(30, 5, iconv("UTF-8", "cp1252", "• ") . 'O: 309 473 8040', 0);
            $pdf->Cell(29, 5, iconv("UTF-8", "cp1252", "• ") . 'F: 309 296 8984', 0);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Generate Schematic PDF
     *
     * @param Submission $submission
     * @param array $selection
     * @return null|string
     */
    public function generateSchematic(Submission $submission, array $selection, $submissionFixtures)
    {
        try {
            // validate parameters
            if (!isset($this->parameters['schematic_path']) || !isset($this->parameters['site_url'])) {
                return null;
            }

            // validate selection
            if (!isset($selection['schematic']) || !isset($selection['btu']) || !isset($selection['quantity'])) {
                return null;
            }

            // validate selected schematic
            if (!file_exists(__DIR__ . '/../../../../web/' . $this->parameters['schematic_path'] . '/' . $selection['schematic'])) {
                return null;
            }

            $pdf = new PDF('P', 'mm', 'A4');

            // generate the first page of new schematic
            if (!$this->generateSchematicDetails($pdf, $submission, $selection, $submissionFixtures)) {
                return null;
            }

            // add selected schematic to new schematic on the second page
            $pdf->AddPage('L');
            $pdf->setSourceFile(__DIR__ . '/../../../../web/' . $this->parameters['schematic_path'] . '/' . $selection['schematic']);
            $tplIdx = $pdf->importPage(1, '/MediaBox');
            $pdf->useTemplate($tplIdx, 0, 0, 297, 210);

            // add submittals
            $submittals = array();

            if (strpos($selection['name'], '+') !== false) {
                foreach (explode('+', preg_replace('/[(]\S*[)]/', '', $selection['name'])) as $selected) {
                    if (trim(strtolower($selected)) == 'iq751') {
                        $submittals[] = array('type' => 'iQ751.pdf', 'submittal' => null);
                    } else if (trim(strtolower($selected)) == 'iq1001') {
                        $submittals[] = array('type' => 'iQ1001.pdf', 'submittal' => null);
                    } else if (trim(strtolower($selected)) == 'iq1501') {
                        $submittals[] = array('type' => 'iQ1501.pdf', 'submittal' => null);
                    }
                }
            } else {
                $tmpName = trim(strtolower(preg_replace('/[(]\S*[)]/', '', $selection['name'])));

                if ($tmpName == 'i200' || $tmpName == 'i250') {
                    $submittals[] = array('type' => 'iSeries.pdf', 'submittal' => $tmpName);
                } else if ($tmpName == 'iq251') {
                    $submittals[] = array('type' => 'iQ251.pdf', 'submittal' => null);
                } else if ($tmpName == 'iq751') {
                    $submittals[] = array('type' => 'iQ751.pdf', 'submittal' => null);
                } else if ($tmpName == 'iq1001') {
                    $submittals[] = array('type' => 'iQ1001.pdf', 'submittal' => null);
                } else if ($tmpName == 'iq1501') {
                    $submittals[] = array('type' => 'iQ1501.pdf', 'submittal' => null);
                } else if ($tmpName == 'iq2001') {
                    $submittals[] = array('type' => 'iQ2001.pdf', 'submittal' => null);
                } else if ($tmpName == 'iq3001') {
                    $submittals[] = array('type' => 'iQ3001.pdf', 'submittal' => null);
                } else if ($tmpName == 'in401') {
                    $submittals[] = array('type' => 'iN401.pdf', 'submittal' => null);
                } else if ($tmpName == 'in501') {
                    $submittals[] = array('type' => 'iN501.pdf', 'submittal' => null);
                }
            }

            foreach ($submittals as $submittal) {
                $pageCount = $pdf->setSourceFile(__DIR__ . '/../../../../web/' . $this->parameters['submittal_path'] . '/' . $submittal['type']);

                for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                    $pdf->AddPage('P');
                    $tplIdx = $pdf->importPage($pageNo, '/MediaBox');
                    $pdf->useTemplate($tplIdx, 0, 0, 210, 297);

                    // draw form
                    if ($pageNo == 1) {
                        $pdf->SetFont('Arial', '', 11);
                        $pdf->SetFillColor(255, 255, 255);

                        $pdf->SetXY(51, 45);
                        $pdf->Cell(51, 7, $submission->getCreated()->format('m/d/Y'), 0, 0, '', true);

                        $pdf->SetXY(51, 56);
                        $pdf->Cell(51, 7, $submission->getProjectName(), 0, 0, '', true);

                        $pdf->SetXY(51, 66.8);
                        $pdf->Cell(51, 7, $submission->getProjectNumber(), 0, 0, '', true);

                        $pdf->SetXY(51, 77.5);
                        $pdf->Cell(26, 7, $submission->getCity(), 0, 0, '', true);
                        $pdf->SetXY(80, 78);
                        $pdf->Cell(6, 6, $submission->getState(), 0, 0, 'C', true);
                        $pdf->SetXY(88.2, 77.5);
                        $pdf->Cell(14, 7, $submission->getZipcode(), 0, 0, '', true);

                        $pdf->SetXY(51, 88.3);
                        $pdf->Cell(51, 7, $submission->getEngineer(), 0, 0, '', true);

                        $pdf->SetXY(51, 99);
                        $pdf->Cell(51, 7, $submission->getContractor(), 0, 0, '', true);

                        if (strtolower($submission->getFuelSupply()) == 'natural') {
                            $pdf->SetXY(139.1, 56.2);
                            $pdf->Cell(6, 6, 'X', 0, 0, 'C', true);
                        } else {
                            $pdf->SetXY(139.1, 56.2);
                            $pdf->Cell(6, 6, '', 0, 0, 'C', true);
                        }

                        if (strtolower($submission->getFuelSupply()) == 'propane') {
                            $pdf->SetXY(169.2, 56.2);
                            $pdf->Cell(6, 6, 'X', 0, 0, 'C', true);
                        } else {
                            $pdf->SetXY(169.2, 56.2);
                            $pdf->Cell(6, 6, '', 0, 0, 'C', true);
                        }

                        if ($submittal['type'] == 'iSeries.pdf') {
                            if ($submittal['submittal'] == 'i200' || $submittal['submittal'] == 'i200p') {
                                $pdf->SetXY(139.1, 67);
                                $pdf->Cell(6, 6, 'X', 0, 0, 'C', true);
                            } else {
                                $pdf->SetXY(139.1, 67);
                                $pdf->Cell(6, 6, '', 0, 0, 'C', true);
                            }

                            if ($submittal['submittal'] == 'i250' || $submittal['submittal'] == 'i250p') {
                                $pdf->SetXY(169.2, 67);
                                $pdf->Cell(6, 6, 'X', 0, 0, 'C', true);
                            } else {
                                $pdf->SetXY(169.2, 67);
                                $pdf->Cell(6, 6, '', 0, 0, 'C', true);
                            }
                        }
                    }
                }
            }

            // add total page number
            $pdf->AliasNbPages();

            // create submission folder for saving schematics
            $destinationFolder = __DIR__ . '/../../../../web/' . $this->parameters['schematic_path'] . '/generated/' . $submission->getId() . '/';

            if (!file_exists($destinationFolder) || !is_dir($destinationFolder)) {
                mkdir($destinationFolder, 0777);
            }

            // create file
            $pdf->Output(__DIR__ . '/../../../../web/' . $this->parameters['schematic_path'] . '/generated/' . $submission->getId() . '/' . $selection['name'] . '.pdf', 'F');

            return $this->parameters['site_url'] . '/' . $this->parameters['schematic_path'] . '/generated/' . $submission->getId() . '/' . $selection['name'] . '.pdf';
        } catch (Exception $e) {
            return null;
        }
    }
}
