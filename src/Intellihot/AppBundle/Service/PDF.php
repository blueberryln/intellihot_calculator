<?php

namespace Intellihot\AppBundle\Service;

use fpdi\FPDI;

class PDF extends FPDI
{
    function Footer()
    {
        $this->SetY(-8);

        $this->SetFont('Arial', '', 10);

        $this->SetTextColor(102, 102, 102);

        // write Page No
        $this->Cell(0, 0, 'Page ' . $this->PageNo() . " of {nb}", 0, 0, 'R');
    }
}