<?php

namespace Intellihot\AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Intellihot\AppBundle\Entity\Option;
use Intellihot\AppBundle\Entity\Submission;
use Intellihot\AppBundle\Entity\Unit;

class Calculator
{
    private $em;
    private $mailer;
    private $pdfGenerator;
    private $contactEmail;

    public function __construct(EntityManager $em, Mailer $mailer, PDFGenerator $pdfGenerator)
    {
        $this->em           = $em;
        $this->mailer       = $mailer;
        $this->pdfGenerator = $pdfGenerator;

        $this->contactEmail = $this->em->getRepository('IntellihotAppBundle:Option')
            ->findOneBy(array('name' => Option::INTELLIHOT_EMAIL));

        if ($this->contactEmail instanceof Option) {
            $this->contactEmail = $this->contactEmail->getValue();
        } else {
            $this->contactEmail = null;
        }
    }

    public function calculate($business, $asme, $mount, $altitude, $waterTempIn, $waterTempOut, $fixtureUnits, $debug = false, $debugBtu = null)
    {
        // Make sure altitude is at least 1
        if (empty($altitude) || $altitude === 0) {
            $altitude = 1;
        }

        $isValid = false;

        // Validation
        if (strlen($business) && strlen($asme) && !empty($mount) && !empty($altitude) && !empty($waterTempIn) && !empty($waterTempOut) && !empty($fixtureUnits)) {
            $isValid = true;
        } else {
            return array(
                'success'  => false,
                'msg'      => "Sorry, we couldn't process your application. Please fill out the form completely.",
                'msg_text' => "Sorry, we couldn't process your application. Please fill out the form completely.",
                'contact'  => $this->contactEmail,
                'gpm'      => 0,
                'btu'      => 0
            );
        }

        if ($isValid === true) {
            // Get entities;
            $altitudeRepo  = $this->em->getRepository('IntellihotAppBundle:Altitude');
            $gpmRepo       = $this->em->getRepository('IntellihotAppBundle:Gpm');
            $selectionRepo = $this->em->getRepository('IntellihotAppBundle:Selection');

            // Query for altitude factor
            $altitudeQuery = $altitudeRepo->createQueryBuilder('c')
                ->select('c')
                ->where('c.altitudeMin < :altitude')
                ->andWhere('c.altitudeMax >= :altitude')
                ->setParameter('altitude', $altitude)
                ->getQuery();

            $altitudeResult = $altitudeQuery->setMaxResults(1)->getResult();

            // Get altitude factor
            if ($altitudeResult) {
                $altitudeFactor = $altitudeResult[0]->getFactor();
            } else {
                return array(
                    'success'  => false,
                    'msg'      => "Sorry, the sizing calculator does not support altitudes greater than 10000ft.<br />Please <a href='mailto:" . $this->contactEmail . "'>contact us</a> to discuss your project requirements.",
                    'msg_text' => "Sorry, the sizing calculator does not support altitudes greater than 10000ft. Please contact us to discuss your project requirements.",
                    'contact'  => $this->contactEmail,
                    'gpm'      => 0,
                    'btu'      => 0
                );
            }

            // Query for minFix, maxFix and highGpm
            $gpmQuery1 = $gpmRepo->createQueryBuilder('c')
                ->select('c')
                ->where('c.fixtureMin < :fixtureUnits')
                ->andWhere('c.fixtureMax >= :fixtureUnits')
                ->setParameter('fixtureUnits', $fixtureUnits)
                ->getQuery();

            $gpmResult1 = $gpmQuery1->setMaxResults(1)->getResult();

            // Get minFix, maxFix and highGpm
            if ($gpmResult1) {
                $gpmGetter = 'getGpm' . $business;
                $minFix    = $gpmResult1[0]->getFixtureMin();
                $maxFix    = $gpmResult1[0]->getFixtureMax();
                $highGpm   = $gpmResult1[0]->$gpmGetter();
            } else {
                return array(
                    'success'  => false,
                    'msg'      => "Sorry, the sizing calculator does not support fixture units greater than 2200fu.<br />Please <a href='mailto:" . $this->contactEmail . "'>contact us</a> to discuss your project requirements.",
                    'msg_text' => "Sorry, the sizing calculator does not support fixture units greater than 2200fu. Please contact us to discuss your project requirements.",
                    'contact'  => $this->contactEmail,
                    'gpm'      => 0,
                    'btu'      => 0
                );
            }

            // Query for lowGpm
            $gpmQuery2 = $gpmRepo->createQueryBuilder('c')
                ->select('c')
                ->where('c.fixtureMax = :minFix')
                ->setParameter('minFix', $minFix)
                ->getQuery();

            $gpmResult2 = $gpmQuery2->getSingleResult();

            // Get lowGpm
            $lowGpm = $gpmResult2->$gpmGetter();

            // Calculate Gpm
            $gpm = $lowGpm + ($highGpm - $lowGpm) * (($fixtureUnits - $minFix) / ($maxFix - $minFix));

            // Calculate Btu
            $btu = 498 * $gpm * ($waterTempOut - $waterTempIn) / $altitudeFactor + 250000;

            // Override btu if debugging is enabled
            if ($debug == true && $debugBtu) {
                $btu = $debugBtu;
            }

            // Query for unit selections
            $selectionQuery  = $selectionRepo->createQueryBuilder('c')
                ->select('c')
                ->where('c.btuMin < :btu')
                ->andWhere('c.btuMax >= :btu')
                ->setParameter('btu', $btu)
                ->getQuery();
            $selectionResult = $selectionQuery->setMaxResults(1)->getResult();

            // Construct getter
            $mountName       = ucfirst($mount);
            $asmeName        = ($asme == 1 ? "Asme" : "NonAsme");
            $selectionGetter = "get" . $mountName . $asmeName;

            // Get unit selections
            if ($selectionResult) {
                $selections = $selectionResult[0]->$selectionGetter();
            } else {
                return array(
                    'success'        => false,
                    'msg'            => "Sorry, we weren't able to generate any recommendations based on these requirements.<br />Please <a href='mailto:" . $this->contactEmail . "'>contact us</a> to discuss your project requirements.",
                    'msg_text'       => "Sorry, we weren't able to generate any recommendations based on these requirements. Please contact us to discuss your project requirements.",
                    'contact'        => $this->contactEmail,
                    'gpm'            => $gpm,
                    'btu'            => $btu,
                    'altitudeFactor' => $altitudeFactor
                );
            }

            // Return data
            return array(
                'success'        => true,
                'gpm'            => $gpm,
                'btu'            => $btu,
                'altitudeFactor' => $altitudeFactor,
                'selections'     => $selections
            );
        }
    }

    /**
     * Get Selection Details
     *
     * @param string $selections A comma separated string containing selection names
     * @return array A multi-dimensional array containing the selection details
     */
    public function getSelectionDetails($projectDetails, $selections)
    {
        // Convert selections string to an array
        $selections = explode(", ", $selections);

        // Get unit entity
        $unitRepo = $this->em->getRepository('IntellihotAppBundle:Unit');

        $selectionDetails = array();

        // Loop through selections and look for name based matches in the unit entity
        foreach ($selections as $selection) {
            $unit = $unitRepo->findOneByName($selection);

            if ($unit instanceof Unit) {
                $type           = "match";
                $name           = trim($unit->getName());
                $quantity       = $unit->getQuantity();
                $btu            = $unit->getBtu();
                $schematic      = $unit->getSchematic();
                $percentage     = (($btu + 250000) / $projectDetails['btu']) * 100;
                $percentagefail = ($btu / $projectDetails['btu']) * 100;
                $image          = $unit->getImage();

            } else { // No match found, set message as name
                $type           = "nomatch";
                $image          = "wall";
                $name           = trim($selection);
                $quantity       = "";
                $btu            = "";
                $schematic      = "";
                $percentage     = "";
                $percentagefail = "";
            }

            $selectionArray = array(
                'type'           => $type,
                'name'           => $name,
                'quantity'       => $quantity,
                'btu'            => $btu,
                'schematic'      => $schematic,
                'percentage'     => $percentage,
                'percentagefail' => $percentagefail,
                'image'          => $image
            );

            array_push($selectionDetails, $selectionArray);
        }

        return $selectionDetails;
    }

    /**
     * Assign Rsm and Representatives to Submission
     *
     * @param Submission $submission
     */
    public function assignRsmAndRepresentativeToSubmission(Submission $submission)
    {
        // Find RSM
        $regionalManager = $this->em->getRepository('IntellihotAppBundle:Rsm')
            ->findOneBy(array('state' => $submission->getState()));

        $submission->setRsm($regionalManager);

        // Find representatives
        $representatives = $this->em->getRepository('IntellihotAppBundle:Representative')
            ->findBy(array('zipcode' => $submission->getZipcode()));

        // remove invalid representatives
        foreach ($representatives as $idx => $rep) {
            if (strlen($rep->getPhone()) < 1 || strlen($rep->getCompany()) < 1 || strlen($rep->getEmail()) < 1 || $rep->getEmail() == null || $rep->getEmail() == 'n/a') {
                unset($representatives[$idx]);
            }
        }

        $submission->setRepresentatives($representatives);

        // Persist submission to database
        $this->em->persist($submission);
        $this->em->flush();
    }

    /**
     * Generate Pdfs for Submission
     *
     * @param Submission $submission
     * @param $submissionFixtures
     * @param $selectionDetails
     * @return array
     */
    public function generatePdfs(Submission $submission, $submissionFixtures, $selectionDetails)
    {
        // get submission fixtures
        if (is_array($submissionFixtures) && count($submissionFixtures) > 0) {
            $fixtures = $this->em->getRepository('IntellihotAppBundle:Fixture')
                ->findBy(array('id' => array_keys($submissionFixtures)));

            foreach ($fixtures as $f) {
                $submissionFixtures[$f->getId()] = $f->getName() . ': ' . $submissionFixtures[$f->getId()];
            }
        }

        // Generate pdfs
        $schematicsDetails = array();
        $newSchematicPaths = array();

        foreach ($selectionDetails as $idx => $selection) {
            $selectionDetails[$idx]['path'] = null;

            if ($selection['type'] == 'match') { // Only process selections with unit matches
                // generate new schematic
                $newSchematicPath = $this->pdfGenerator->generateSchematic($submission, $selection, $submissionFixtures);

                if ($newSchematicPath != null) {
                    $schematicsArray = array(
                        'name' => $selection['name'],
                        'path' => $newSchematicPath
                    );

                    $selectionDetails[$idx]['path'] = $newSchematicPath;
                    $newSchematicPaths[]            = $newSchematicPath;
                    array_push($schematicsDetails, $schematicsArray);
                }
            }
        }

        // Persist links to the new schematics
        try {
            $submission->setSchematics(implode("\n", $newSchematicPaths));
            $this->em->flush();
        } catch (\Exception $e) {
        }

        // Send success email
        if (sizeof($schematicsDetails) > 0) {
            $this->mailer->sendSuccessEmail($submission, $selectionDetails);
        }

        return $schematicsDetails;
    }
}