<?php

namespace Intellihot\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Submission
 *
 * @ORM\Table(name="submission")
 * @ORM\Entity
 */
class Submission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"full_step"})
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank(groups={"full_step"})
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(groups={"full_step"})
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @var string
     * @Assert\NotBlank(groups={"full_step"})
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     * @Assert\NotBlank(groups={"full_step"})
     * @ORM\Column(name="state", type="string", length=255)
     */
    private $state;

    /**
     * @var string
     * @Assert\NotBlank(groups={"full_step"})
     * @ORM\Column(name="zipcode", type="string", length=255)
     */
    private $zipcode;

    /**
     * @var string
     * @Assert\NotBlank(groups={"full_step"})
     * @ORM\Column(name="project_name", type="string", length=255)
     */
    private $projectName;

    /**
     * @var string
     * @ORM\Column(name="project_number", type="string", length=255, nullable=true)
     */
    private $projectNumber;

    /**
     * @var string
     * @Assert\NotBlank(groups={"full_step"})
     * @ORM\Column(name="project_description", type="string", length=255)
     */
    private $projectDescription;

    /**
     * @var string
     * @ORM\Column(name="additional_comments", type="string", length=255, nullable=true)
     */
    private $additionalComments;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Intellihot\AppBundle\Entity\Business")
     * @ORM\JoinColumn(name="business", referencedColumnName="id")
     */
    private $business;

    /**
     * @var boolean
     * @ORM\Column(name="asme_requirement", type="boolean", nullable=true)
     */
    private $asmeRequirement;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="mounting_option", type="string", length=255)
     */
    private $mountingOption;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="fuel_supply", type="string", length=255)
     */
    private $fuelSupply;

    /**
     * @var string
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "Altitude must be at least {{ limit }}",
     * )
     * @ORM\Column(name="altitude", type="integer", nullable=true)
     */
    private $altitude;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 35,
     *      minMessage = "Incoming water temperature (°F) must be at least {{ limit }}",
     * )
     * @ORM\Column(name="incoming_water_temperature", type="integer")
     */
    private $incomingWaterTemperature;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 1,
     *      max = 180,
     *      minMessage = "Desired output temperature (°F) must be at least {{ limit }}",
     *      maxMessage = "Desired output temperature (°F) cannot be higher than {{ limit }}"
     * )
     * @ORM\Column(name="desired_ouput_temperature", type="integer")
     */
    private $desiredOutputTemperature;

    /**
     * @var integer
     * @Assert\NotBlank(groups={"full_step"})
     * @Assert\Range(
     *      min = 1,
     *      max = 180,
     *      minMessage = "Point temperature (°F) must be at least {{ limit }}",
     *      maxMessage = "Point temperature (°F) cannot be higher than {{ limit }}"
     * )
     * @ORM\Column(name="point_temperature", type="integer")
     */
    private $pointTemperature;

    /**
     * @var integer
     *
     * @ORM\Column(name="rooms", type="integer", nullable=true)
     */
    private $rooms;

    /**
     * @var string
     *
     * @ORM\Column(name="fixture_details", type="string", length=255, nullable=true)
     */
    private $fixtureDetails;

    /**
     * @var float
     *
     * @ORM\Column(name="fixture_units", type="float", precision=10, scale=0, nullable=true)
     */
    private $fixtureUnits;

    /**
     * @var float
     *
     * @ORM\Column(name="altitude_factor", type="float", precision=10, scale=0, nullable=true)
     */
    private $altitudeFactor;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm;

    /**
     * @var float
     *
     * @ORM\Column(name="btu", type="float", precision=10, scale=0, nullable=true)
     */
    private $btu;

    /**
     * @var string
     *
     * @ORM\Column(name="selections", type="string", length=255, nullable=true)
     */
    private $selections;

    /**
     * @var string
     *
     * @ORM\Column(name="schematics", type="string", length=1024, nullable=true)
     */
    private $schematics;

    /**
     * @ORM\ManyToOne(targetEntity="Intellihot\AppBundle\Entity\Rsm")
     * @ORM\JoinColumn(name="rsm", referencedColumnName="id")
     */
    private $rsm;

    /**
     * @ORM\ManyToMany(targetEntity="Intellihot\AppBundle\Entity\Representative")
     * @ORM\JoinTable(name="submissions_representatives",
     *      joinColumns={@ORM\JoinColumn(name="submission", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="representative", referencedColumnName="id")}
     * )
     */
    private $representatives;

    /**
     * @var string
     * @ORM\Column(name="engineer", type="string", length=255, nullable=true)
     */
    private $engineer;

    /**
     * @var string
     * @ORM\Column(name="contractor", type="string", length=255, nullable=true)
     */
    private $contractor;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->representatives = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Submission
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Submission
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Submission
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Submission
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Submission
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     * @return Submission
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set projectName
     *
     * @param string $projectName
     * @return Submission
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;

        return $this;
    }

    /**
     * Get projectName
     *
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set projectNumber
     *
     * @param string $projectNumber
     * @return Submission
     */
    public function setProjectNumber($projectNumber)
    {
        $this->projectNumber = $projectNumber;

        return $this;
    }

    /**
     * Get projectNumber
     *
     * @return string
     */
    public function getProjectNumber()
    {
        return $this->projectNumber;
    }

    /**
     * Set projectDescription
     *
     * @param string $projectDescription
     * @return Submission
     */
    public function setProjectDescription($projectDescription)
    {
        $this->projectDescription = $projectDescription;

        return $this;
    }

    /**
     * Get projectDescription
     *
     * @return string
     */
    public function getProjectDescription()
    {
        return $this->projectDescription;
    }

    /**
     * Set additionalComments
     *
     * @param string $additionalComments
     * @return Submission
     */
    public function setAdditionalComments($additionalComments)
    {
        $this->additionalComments = $additionalComments;

        return $this;
    }

    /**
     * Get additionalComments
     *
     * @return string
     */
    public function getAdditionalComments()
    {
        return $this->additionalComments;
    }

    /**
     * Set asmeRequirement
     *
     * @param boolean $asmeRequirement
     * @return Submission
     */
    public function setAsmeRequirement($asmeRequirement)
    {
        $this->asmeRequirement = $asmeRequirement;

        return $this;
    }

    /**
     * Get asmeRequirement
     *
     * @return boolean
     */
    public function getAsmeRequirement()
    {
        return $this->asmeRequirement;
    }

    /**
     * Set mountingOption
     *
     * @param string $mountingOption
     * @return Submission
     */
    public function setMountingOption($mountingOption)
    {
        $this->mountingOption = $mountingOption;

        return $this;
    }

    /**
     * Get mountingOption
     *
     * @return string
     */
    public function getMountingOption()
    {
        return $this->mountingOption;
    }

    /**
     * Set fuelSupply
     *
     * @param string $fuelSupply
     * @return Submission
     */
    public function setFuelSupply($fuelSupply)
    {
        $this->fuelSupply = $fuelSupply;

        return $this;
    }

    /**
     * Get fuelSupply
     *
     * @return string
     */
    public function getFuelSupply()
    {
        return $this->fuelSupply;
    }

    /**
     * Set altitude
     *
     * @param integer $altitude
     * @return Submission
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;

        return $this;
    }

    /**
     * Get altitude
     *
     * @return integer
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * Set incomingWaterTemperature
     *
     * @param integer $incomingWaterTemperature
     * @return Submission
     */
    public function setIncomingWaterTemperature($incomingWaterTemperature)
    {
        $this->incomingWaterTemperature = $incomingWaterTemperature;

        return $this;
    }

    /**
     * Get incomingWaterTemperature
     *
     * @return integer
     */
    public function getIncomingWaterTemperature()
    {
        return $this->incomingWaterTemperature;
    }

    /**
     * Set desiredOutputTemperature
     *
     * @param integer $desiredOutputTemperature
     * @return Submission
     */
    public function setDesiredOutputTemperature($desiredOutputTemperature)
    {
        $this->desiredOutputTemperature = $desiredOutputTemperature;

        return $this;
    }

    /**
     * Get desiredOutputTemperature
     *
     * @return integer
     */
    public function getDesiredOutputTemperature()
    {
        return $this->desiredOutputTemperature;
    }

    /**
     * Set pointTemperature
     *
     * @param integer $pointTemperature
     * @return Submission
     */
    public function setPointTemperature($pointTemperature)
    {
        $this->pointTemperature = $pointTemperature;

        return $this;
    }

    /**
     * Get pointTemperature
     *
     * @return integer
     */
    public function getPointTemperature()
    {
        return $this->pointTemperature;
    }

    /**
     * Set rooms
     *
     * @param integer $rooms
     * @return Submission
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     * Get rooms
     *
     * @return integer
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set fixtureDetails
     *
     * @param string $fixtureDetails
     * @return Submission
     */
    public function setFixtureDetails($fixtureDetails)
    {
        $this->fixtureDetails = $fixtureDetails;

        return $this;
    }

    /**
     * Get fixtureDetails
     *
     * @return string
     */
    public function getFixtureDetails()
    {
        return $this->fixtureDetails;
    }

    /**
     * Set fixtureUnits
     *
     * @param float $fixtureUnits
     * @return Submission
     */
    public function setFixtureUnits($fixtureUnits)
    {
        $this->fixtureUnits = $fixtureUnits;

        return $this;
    }

    /**
     * Get fixtureUnits
     *
     * @return float
     */
    public function getFixtureUnits()
    {
        return $this->fixtureUnits;
    }

    /**
     * Set altitudeFactor
     *
     * @param float $altitudeFactor
     * @return Submission
     */
    public function setAltitudeFactor($altitudeFactor)
    {
        $this->altitudeFactor = $altitudeFactor;

        return $this;
    }

    /**
     * Get altitudeFactor
     *
     * @return float
     */
    public function getAltitudeFactor()
    {
        return $this->altitudeFactor;
    }

    /**
     * Set gpm
     *
     * @param float $gpm
     * @return Submission
     */
    public function setGpm($gpm)
    {
        $this->gpm = $gpm;

        return $this;
    }

    /**
     * Get gpm
     *
     * @return float
     */
    public function getGpm()
    {
        return $this->gpm;
    }

    /**
     * Set btu
     *
     * @param float $btu
     * @return Submission
     */
    public function setBtu($btu)
    {
        $this->btu = $btu;

        return $this;
    }

    /**
     * Get btu
     *
     * @return float
     */
    public function getBtu()
    {
        return $this->btu;
    }

    /**
     * Set selections
     *
     * @param string $selections
     * @return Submission
     */
    public function setSelections($selections)
    {
        $this->selections = $selections;

        return $this;
    }

    /**
     * Get selections
     *
     * @return string
     */
    public function getSelections()
    {
        return $this->selections;
    }

    /**
     * Set schematics
     *
     * @param string $schematics
     * @return Submission
     */
    public function setSchematics($schematics)
    {
        $this->schematics = $schematics;

        return $this;
    }

    /**
     * Get schematics
     *
     * @return string
     */
    public function getSchematics()
    {
        return $this->schematics;
    }

    /**
     * Set engineer
     *
     * @param string $engineer
     * @return Submission
     */
    public function setEngineer($engineer)
    {
        $this->engineer = $engineer;

        return $this;
    }

    /**
     * Get engineer
     *
     * @return string
     */
    public function getEngineer()
    {
        return $this->engineer;
    }

    /**
     * Set contractor
     *
     * @param string $contractor
     * @return Submission
     */
    public function setContractor($contractor)
    {
        $this->contractor = $contractor;

        return $this;
    }

    /**
     * Get contractor
     *
     * @return string
     */
    public function getContractor()
    {
        return $this->contractor;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Submission
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set business
     *
     * @param \Intellihot\AppBundle\Entity\Business $business
     * @return Submission
     */
    public function setBusiness(\Intellihot\AppBundle\Entity\Business $business = null)
    {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business
     *
     * @return \Intellihot\AppBundle\Entity\Business
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * Set rsm
     *
     * @param \Intellihot\AppBundle\Entity\Rsm $rsm
     * @return Submission
     */
    public function setRsm(\Intellihot\AppBundle\Entity\Rsm $rsm = null)
    {
        $this->rsm = $rsm;

        return $this;
    }

    /**
     * Get rsm
     *
     * @return \Intellihot\AppBundle\Entity\Rsm
     */
    public function getRsm()
    {
        return $this->rsm;
    }

    /**
     * Get representatives
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepresentatives()
    {
        return $this->representatives;
    }

    public function setRepresentatives($representatives)
    {
        $this->representatives = $representatives;

        return $this;
    }

    /**
     * Add representatives
     *
     * @param \Intellihot\AppBundle\Entity\Representative $representatives
     * @return Submission
     */
    public function addRepresentative(\Intellihot\AppBundle\Entity\Representative $representatives)
    {
        $this->representatives[] = $representatives;

        return $this;
    }

    /**
     * Remove representatives
     *
     * @param \Intellihot\AppBundle\Entity\Representative $representatives
     */
    public function removeRepresentative(\Intellihot\AppBundle\Entity\Representative $representatives)
    {
        $this->representatives->removeElement($representatives);
    }
}
