<?php

namespace Intellihot\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gpm
 *
 * @ORM\Table(name="gpm")
 * @ORM\Entity
 */
class Gpm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="fixture_max", type="float", precision=10, scale=0, nullable=false)
     */
    private $fixtureMax;

    /**
     * @var float
     *
     * @ORM\Column(name="fixture_min", type="float", precision=10, scale=0, nullable=false)
     */
    private $fixtureMin;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm0", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm0;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm1", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm1;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm2", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm2;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm3", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm3;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm4", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm4;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm5", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm5;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm6", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm6;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm7", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm7;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm8", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm8;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm9", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm9;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm10", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm10;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm11", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm11;

    /**
     * @var float
     *
     * @ORM\Column(name="gpm12", type="float", precision=10, scale=0, nullable=true)
     */
    private $gpm12;

    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fixtureMax
     *
     * @param float $fixtureMax
     * @return Gpm
     */
    public function setFixtureMax($fixtureMax)
    {
        $this->fixtureMax = $fixtureMax;

        return $this;
    }

    /**
     * Get fixtureMax
     *
     * @return float
     */
    public function getFixtureMax()
    {
        return $this->fixtureMax;
    }

    /**
     * Set fixtureMin
     *
     * @param float $fixtureMin
     * @return Gpm
     */
    public function setFixtureMin($fixtureMin)
    {
        $this->fixtureMin = $fixtureMin;

        return $this;
    }

    /**
     * Get fixtureMin
     *
     * @return float
     */
    public function getFixtureMin()
    {
        return $this->fixtureMin;
    }

    /**
     * Set gpm0
     *
     * @param float $gpm0
     * @return Gpm
     */
    public function setGpm0($gpm0)
    {
        $this->gpm0 = $gpm0;

        return $this;
    }

    /**
     * Get gpm0
     *
     * @return float
     */
    public function getGpm0()
    {
        return $this->gpm0;
    }

    /**
     * Set gpm1
     *
     * @param float $gpm1
     * @return Gpm
     */
    public function setGpm1($gpm1)
    {
        $this->gpm1 = $gpm1;

        return $this;
    }

    /**
     * Get gpm1
     *
     * @return float
     */
    public function getGpm1()
    {
        return $this->gpm1;
    }

    /**
     * Set gpm2
     *
     * @param float $gpm2
     * @return Gpm
     */
    public function setGpm2($gpm2)
    {
        $this->gpm2 = $gpm2;

        return $this;
    }

    /**
     * Get gpm2
     *
     * @return float
     */
    public function getGpm2()
    {
        return $this->gpm2;
    }

    /**
     * Set gpm3
     *
     * @param float $gpm3
     * @return Gpm
     */
    public function setGpm3($gpm3)
    {
        $this->gpm3 = $gpm3;

        return $this;
    }

    /**
     * Get gpm3
     *
     * @return float
     */
    public function getGpm3()
    {
        return $this->gpm3;
    }

    /**
     * Set gpm4
     *
     * @param float $gpm4
     * @return Gpm
     */
    public function setGpm4($gpm4)
    {
        $this->gpm4 = $gpm4;

        return $this;
    }

    /**
     * Get gpm4
     *
     * @return float
     */
    public function getGpm4()
    {
        return $this->gpm4;
    }

    /**
     * Set gpm5
     *
     * @param float $gpm5
     * @return Gpm
     */
    public function setGpm5($gpm5)
    {
        $this->gpm5 = $gpm5;

        return $this;
    }

    /**
     * Get gpm5
     *
     * @return float
     */
    public function getGpm5()
    {
        return $this->gpm5;
    }

    /**
     * Set gpm6
     *
     * @param float $gpm6
     * @return Gpm
     */
    public function setGpm6($gpm6)
    {
        $this->gpm6 = $gpm6;

        return $this;
    }

    /**
     * Get gpm6
     *
     * @return float
     */
    public function getGpm6()
    {
        return $this->gpm6;
    }

    /**
     * Set gpm7
     *
     * @param float $gpm7
     * @return Gpm
     */
    public function setGpm7($gpm7)
    {
        $this->gpm7 = $gpm7;

        return $this;
    }

    /**
     * Get gpm7
     *
     * @return float
     */
    public function getGpm7()
    {
        return $this->gpm7;
    }

    /**
     * Set gpm8
     *
     * @param float $gpm8
     * @return Gpm
     */
    public function setGpm8($gpm8)
    {
        $this->gpm8 = $gpm8;

        return $this;
    }

    /**
     * Get gpm8
     *
     * @return float
     */
    public function getGpm8()
    {
        return $this->gpm8;
    }

    /**
     * Set gpm9
     *
     * @param float $gpm9
     * @return Gpm
     */
    public function setGpm9($gpm9)
    {
        $this->gpm9 = $gpm9;

        return $this;
    }

    /**
     * Get gpm9
     *
     * @return float
     */
    public function getGpm9()
    {
        return $this->gpm9;
    }

    /**
     * Set gpm10
     *
     * @param float $gpm10
     * @return Gpm
     */
    public function setGpm10($gpm10)
    {
        $this->gpm10 = $gpm10;

        return $this;
    }

    /**
     * Get gpm10
     *
     * @return float
     */
    public function getGpm10()
    {
        return $this->gpm10;
    }

    /**
     * Set gpm11
     *
     * @param float $gpm11
     * @return Gpm
     */
    public function setGpm11($gpm11)
    {
        $this->gpm11 = $gpm11;

        return $this;
    }

    /**
     * Get gpm11
     *
     * @return float
     */
    public function getGpm11()
    {
        return $this->gpm11;
    }

    /**
     * Set gpm12
     *
     * @param float $gpm12
     * @return Gpm
     */
    public function setGpm12($gpm12)
    {
        $this->gpm12 = $gpm12;

        return $this;
    }

    /**
     * Get gpm12
     *
     * @return float
     */
    public function getGpm12()
    {
        return $this->gpm12;
    }
}
