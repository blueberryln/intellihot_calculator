<?php

namespace Intellihot\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fixture
 *
 * @ORM\Table(name="fixture")
 * @ORM\Entity
 */
class Fixture
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float", precision=10, scale=0, nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="Intellihot\AppBundle\Entity\Business", inversedBy="fixtures")
     * @ORM\JoinColumn(name="business", referencedColumnName="id")
     */
    private $business;

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Fixture
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return Fixture
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set business
     *
     * @param \Intellihot\AppBundle\Entity\Business $business
     * @return Fixture
     */
    public function setBusiness(\Intellihot\AppBundle\Entity\Business $business = null)
    {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business
     *
     * @return \Intellihot\AppBundle\Entity\Business
     */
    public function getBusiness()
    {
        return $this->business;
    }

    public function getBusinessId()
    {
        return $this->business ? $this->business->getId() : 0;
    }

    public function isAutoPopulate()
    {
        $slug = 'fixture-' . str_replace(' ', '-', strtolower($this->getName()));

        if (in_array($slug, array('fixture-private-lavatory', 'fixture-shower-and-tub'))) {
            return true;
        }

        if ($this->getBusinessId() != 0 && $this->getBusinessId() != 12) {
            if (in_array($slug, array('fixture-domestic-kitchen-sink'))) {
                return true;
            }
        }

        return false;
    }
}
