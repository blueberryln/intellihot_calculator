<?php

namespace Intellihot\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Business
 *
 * @ORM\Table(name="business")
 * @ORM\Entity
 */
class Business
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Intellihot\AppBundle\Entity\Fixture", mappedBy="business")
     **/
    private $fixtures;

    /**
     * @var boolean
     * @ORM\Column(name="has_rooms", type="boolean", options={"default" = 0})
     */
    private $hasRooms;

    /**
     * @var string
     *
     * @ORM\Column(name="notification_emails", type="string", length=255, nullable=true)
     */
    private $notificationEmails;

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fixtures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Business
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set hasRooms
     *
     * @param boolean $hasRooms
     * @return Business
     */
    public function setHasRooms($hasRooms)
    {
        $this->hasRooms = $hasRooms;

        return $this;
    }

    /**
     * Get hasRooms
     *
     * @return boolean
     */
    public function getHasRooms()
    {
        return $this->hasRooms;
    }

    /**
     * Add fixtures
     *
     * @param \Intellihot\AppBundle\Entity\Fixture $fixtures
     * @return Business
     */
    public function addFixture(\Intellihot\AppBundle\Entity\Fixture $fixtures)
    {
        $this->fixtures[] = $fixtures;

        return $this;
    }

    /**
     * Remove fixtures
     *
     * @param \Intellihot\AppBundle\Entity\Fixture $fixtures
     */
    public function removeFixture(\Intellihot\AppBundle\Entity\Fixture $fixtures)
    {
        $this->fixtures->removeElement($fixtures);
    }

    /**
     * Get fixtures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFixtures()
    {
        return $this->fixtures;
    }

    /**
     * Set notificationEmails
     *
     * @param string $notificationEmails
     * @return Business
     */
    public function setNotificationEmails($notificationEmails)
    {
        $this->notificationEmails = $notificationEmails;

        return $this;
    }

    /**
     * Get notificationEmails
     *
     * @return string
     */
    public function getNotificationEmails()
    {
        return $this->notificationEmails;
    }
}
