<?php

namespace Intellihot\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Selection
 *
 * @ORM\Table(name="selection")
 * @ORM\Entity
 */
class Selection
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="btu_min", type="integer", nullable=false)
     */
    private $btuMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="btu_max", type="integer", nullable=false)
     */
    private $btuMax;

    /**
     * @var string
     * @ORM\Column(name="wall_non_asme", type="string", length=255, nullable=true)
     */
    private $wallNonAsme;

    /**
     * @var string
     * @ORM\Column(name="floor_non_asme", type="string", length=255, nullable=true)
     */
    private $floorNonAsme;

    /**
     * @var string
     * @ORM\Column(name="either_non_asme", type="string", length=255, nullable=true)
     */
    private $eitherNonAsme;

    /**
     * @var string
     * @ORM\Column(name="wall_asme", type="string", length=255, nullable=true)
     */
    private $wallAsme;

    /**
     * @var string
     * @ORM\Column(name="floor_asme", type="string", length=255, nullable=true)
     */
    private $floorAsme;

    /**
     * @var string
     * @ORM\Column(name="either_asme", type="string", length=255, nullable=true)
     */
    private $eitherAsme;

    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set btuMin
     *
     * @param integer $btuMin
     * @return Selection
     */
    public function setBtuMin($btuMin)
    {
        $this->btuMin = $btuMin;

        return $this;
    }

    /**
     * Get btuMin
     *
     * @return integer
     */
    public function getBtuMin()
    {
        return $this->btuMin;
    }

    /**
     * Set btuMax
     *
     * @param integer $btuMax
     * @return Selection
     */
    public function setBtuMax($btuMax)
    {
        $this->btuMax = $btuMax;

        return $this;
    }

    /**
     * Get btuMax
     *
     * @return integer
     */
    public function getBtuMax()
    {
        return $this->btuMax;
    }

    /**
     * Set wallNonAsme
     *
     * @param string $wallNonAsme
     * @return Selection
     */
    public function setWallNonAsme($wallNonAsme)
    {
        $this->wallNonAsme = $wallNonAsme;

        return $this;
    }

    /**
     * Get wallNonAsme
     *
     * @return string
     */
    public function getWallNonAsme()
    {
        return $this->wallNonAsme;
    }

    /**
     * Set floorNonAsme
     *
     * @param string $floorNonAsme
     * @return Selection
     */
    public function setFloorNonAsme($floorNonAsme)
    {
        $this->floorNonAsme = $floorNonAsme;

        return $this;
    }

    /**
     * Get floorNonAsme
     *
     * @return string
     */
    public function getFloorNonAsme()
    {
        return $this->floorNonAsme;
    }

    /**
     * Set eitherNonAsme
     *
     * @param string $eitherNonAsme
     * @return Selection
     */
    public function setEitherNonAsme($eitherNonAsme)
    {
        $this->eitherNonAsme = $eitherNonAsme;

        return $this;
    }

    /**
     * Get eitherNonAsme
     *
     * @return string
     */
    public function getEitherNonAsme()
    {
        return $this->eitherNonAsme;
    }

    /**
     * Set wallAsme
     *
     * @param string $wallAsme
     * @return Selection
     */
    public function setWallAsme($wallAsme)
    {
        $this->wallAsme = $wallAsme;

        return $this;
    }

    /**
     * Get wallAsme
     *
     * @return string
     */
    public function getWallAsme()
    {
        return $this->wallAsme;
    }

    /**
     * Set floorAsme
     *
     * @param string $floorAsme
     * @return Selection
     */
    public function setFloorAsme($floorAsme)
    {
        $this->floorAsme = $floorAsme;

        return $this;
    }

    /**
     * Get floorAsme
     *
     * @return string
     */
    public function getFloorAsme()
    {
        return $this->floorAsme;
    }

    /**
     * Set eitherAsme
     *
     * @param string $eitherAsme
     * @return Selection
     */
    public function setEitherAsme($eitherAsme)
    {
        $this->eitherAsme = $eitherAsme;

        return $this;
    }

    /**
     * Get eitherAsme
     *
     * @return string
     */
    public function getEitherAsme()
    {
        return $this->eitherAsme;
    }
}
