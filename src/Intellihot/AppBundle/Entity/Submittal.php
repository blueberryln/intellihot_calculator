<?php

namespace Intellihot\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Submittal
 *
 * @ORM\Table(name="submittal")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Submittal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @Assert\File(
     *     maxSize="10M",
     *     mimeTypes={"application/pdf"}
     * )
     * @Vich\UploadableField(mapping="submittal_file", fileNameProperty="submittal")
     *
     * @var File $submittalFile
     */
    protected $submittalFile;

    /**
     * @var string
     *
     * @ORM\Column(name="submittal", type="string", length=255)
     */
    private $submittal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->updatedAt = new \DateTime();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Submittal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set submittal
     *
     * @param string $submittal
     * @return Submittal
     */
    public function setSubmittal($submittal)
    {
        $this->submittal = $submittal;

        return $this;
    }

    /**
     * Get submittal
     *
     * @return string
     */
    public function getSubmittal()
    {
        return $this->submittal;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Submittal
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set submittalFile
     *
     * @param File $submittalFile
     * @return Submittal
     */
    public function setSubmittalFile($submittalFile)
    {
        $this->submittalFile = $submittalFile;

        if ($this->submittalFile) {
            $this->updatedAt = new \DateTime();
            $this->submittal = $this->getName();
        }

        return $this;
    }

    /**
     * Get submittalFile
     *
     * @return File
     */
    public function getSubmittalFile()
    {
        return $this->submittalFile;
    }
}
