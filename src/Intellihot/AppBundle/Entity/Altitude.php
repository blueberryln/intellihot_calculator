<?php

namespace Intellihot\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Altitude
 *
 * @ORM\Table(name="altitude")
 * @ORM\Entity
 */
class Altitude
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="altitude_min", type="integer", nullable=false)
     */
    private $altitudeMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="altitude_max", type="integer", nullable=false)
     */
    private $altitudeMax;

    /**
     * @var float
     *
     * @ORM\Column(name="factor", type="float", precision=10, scale=0, nullable=false)
     */
    private $factor;

    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set altitudeMin
     *
     * @param integer $altitudeMin
     * @return Altitude
     */
    public function setAltitudeMin($altitudeMin)
    {
        $this->altitudeMin = $altitudeMin;

        return $this;
    }

    /**
     * Get altitudeMin
     *
     * @return integer
     */
    public function getAltitudeMin()
    {
        return $this->altitudeMin;
    }

    /**
     * Set altitudeMax
     *
     * @param integer $altitudeMax
     * @return Altitude
     */
    public function setAltitudeMax($altitudeMax)
    {
        $this->altitudeMax = $altitudeMax;

        return $this;
    }

    /**
     * Get altitudeMax
     *
     * @return integer
     */
    public function getAltitudeMax()
    {
        return $this->altitudeMax;
    }

    /**
     * Set factor
     *
     * @param float $factor
     * @return Altitude
     */
    public function setFactor($factor)
    {
        $this->factor = $factor;

        return $this;
    }

    /**
     * Get factor
     *
     * @return float
     */
    public function getFactor()
    {
        return $this->factor;
    }
}
