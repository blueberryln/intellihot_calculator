<?php

namespace Intellihot\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Unit
 *
 * @ORM\Table(name="unit")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Unit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * @Assert\File(
     *     maxSize="10M",
     *     mimeTypes={"application/pdf"}
     * )
     * @Vich\UploadableField(mapping="schematic_file", fileNameProperty="schematic")
     *
     * @var File $schematicFile
     */
    protected $schematicFile;

    /**
     * @var string
     *
     * @ORM\Column(name="schematic", type="string", length=255)
     */
    private $schematic;

    /**
     * @var integer
     *
     * @ORM\Column(name="btu", type="integer")
     */
    private $btu;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=10)
     */
    private $image;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->updatedAt = new \DateTime();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Unit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Unit
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set btu
     *
     * @param integer $btu
     * @return Unit
     */
    public function setBtu($btu)
    {
        $this->btu = $btu;

        return $this;
    }

    /**
     * Get btu
     *
     * @return integer
     */
    public function getBtu()
    {
        return $this->btu;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Unit
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set schematic
     *
     * @param string $schematic
     * @return Unit
     */
    public function setSchematic($schematic)
    {
        $this->schematic = $schematic;

        return $this;
    }

    /**
     * Get schematic
     *
     * @return string
     */
    public function getSchematic()
    {
        return $this->schematic;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Unit
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set schematicFile
     *
     * @param File $schematicFile
     * @return Unit
     */
    public function setSchematicFile($schematicFile)
    {
        $this->schematicFile = $schematicFile;

        if ($this->schematicFile) {
            $this->updatedAt = new \DateTime();
        }

        return $this;
    }

    /**
     * Get schematicFile
     *
     * @return File
     */
    public function getSchematicFile()
    {
        return $this->schematicFile;
    }
}
