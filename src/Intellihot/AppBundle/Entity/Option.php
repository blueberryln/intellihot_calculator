<?php

namespace Intellihot\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Option
 *
 * @ORM\Table(name="options", uniqueConstraints={@ORM\UniqueConstraint(name="idx_name", columns={"name"})})
 * @ORM\Entity
 */
class Option
{

    const MANAGER_EMAIL_ROUTING     = 'manager_email_routing';
    const MANAGER_EMAIL_ROUTING_ON  = 'on';
    const MANAGER_EMAIL_ROUTING_OFF = 'off';

    const REPRESENTATIVE_EMAIL_ROUTING     = 'representative_email_routing';
    const REPRESENTATIVE_EMAIL_ROUTING_ON  = 'on';
    const REPRESENTATIVE_EMAIL_ROUTING_OFF = 'off';

    const INTELLIHOT_EMAIL_ROUTING     = 'intellihot_email_routing';
    const INTELLIHOT_EMAIL_ROUTING_ON  = 'on';
    const INTELLIHOT_EMAIL_ROUTING_OFF = 'off';

    const INTELLIHOT_EMAIL      = 'intellihot_email';
    const INTELLIHOT_EMAIL_NAME = 'intellihot_email_name';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text")
     */
    private $value;

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Option
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Option
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Option
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
