<?php

namespace Intellihot\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Intellihot\AppBundle\Entity\Option;

class LoadOptionData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $newOption = new Option();
        $newOption->setName(Option::MANAGER_EMAIL_ROUTING)->setLabel('Regional Sales Manager Email Routing')->setValue(Option::MANAGER_EMAIL_ROUTING_OFF);
        $manager->persist($newOption);

        $newOption = new Option();
        $newOption->setName(Option::REPRESENTATIVE_EMAIL_ROUTING)->setLabel('Representatives Email Routing')->setValue(Option::REPRESENTATIVE_EMAIL_ROUTING_OFF);
        $manager->persist($newOption);

        $newOption = new Option();
        $newOption->setName(Option::INTELLIHOT_EMAIL_ROUTING)->setLabel('Intellihot Email Routing')->setValue(Option::INTELLIHOT_EMAIL_ROUTING_ON);
        $manager->persist($newOption);

        $newOption = new Option();
        $newOption->setName(Option::INTELLIHOT_EMAIL)->setLabel('Intellihot Email Address')->setValue('calculator@intellihot.com');
        $manager->persist($newOption);

        $newOption = new Option();
        $newOption->setName(Option::INTELLIHOT_EMAIL_NAME)->setLabel('Intellihot Email Name')->setValue('Intellihot');
        $manager->persist($newOption);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}