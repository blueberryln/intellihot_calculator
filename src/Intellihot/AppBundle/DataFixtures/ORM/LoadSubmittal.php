<?php

namespace Intellihot\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Intellihot\AppBundle\Entity\Submittal;

class LoadSubmittal extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $submittals = array('iQ251.pdf', 'iQ751.pdf', 'iQ1001.pdf', 'iQ1501.pdf', 'iQ2001.pdf', 'iQ3001.pdf', 'iSeries.pdf');

        foreach ($submittals as $sub) {
            $newSubmittal = new Submittal();
            $newSubmittal->setName($sub)->setSubmittal($sub);
            $manager->persist($newSubmittal);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}