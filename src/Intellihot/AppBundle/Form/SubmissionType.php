<?php

namespace Intellihot\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityRepository;

class SubmissionType extends AbstractType
{
    private $api;

    private $isPreview;

    public static $states = array(
        'AL' => "Alabama, US",
        'AK' => "Alaska, US",
        'AZ' => "Arizona, US",
        'AR' => "Arkansas, US",
        'CA' => "California, US",
        'CO' => "Colorado, US",
        'CT' => "Connecticut, US",
        'DE' => "Delaware, US",
        'DC' => "District of Columbia, US",
        'FL' => "Florida, US",
        'GA' => "Georgia, US",
        'HI' => "Hawaii, US",
        'ID' => "Idaho, US",
        'IL' => "Illinois, US",
        'IN' => "Indiana, US",
        'IA' => "Iowa, US",
        'KS' => "Kansas, US",
        'KY' => "Kentucky, US",
        'LA' => "Louisiana, US",
        'ME' => "Maine, US",
        'MD' => "Maryland, US",
        'MA' => "Massachusetts, US",
        'MI' => "Michigan, US",
        'MN' => "Minnesota, US",
        'MS' => "Mississippi, US",
        'MO' => "Missouri, US",
        'MT' => "Montana, US",
        'NE' => "Nebraska, US",
        'NV' => "Nevada, US",
        'NH' => "New Hampshire, US",
        'NJ' => "New Jersey, US",
        'NM' => "New Mexico, US",
        'NY' => "New York, US",
        'NC' => "North Carolina, US",
        'ND' => "North Dakota, US",
        'OH' => "Ohio, US",
        'OK' => "Oklahoma, US",
        'OR' => "Oregon, US",
        'PA' => "Pennsylvania, US",
        'RI' => "Rhode Island, US",
        'SC' => "South Carolina, US",
        'SD' => "South Dakota, US",
        'TN' => "Tennessee, US",
        'TX' => "Texas, US",
        'UT' => "Utah, US",
        'VT' => "Vermont, US",
        'VA' => "Virginia, US",
        'WA' => "Washington, US",
        'WV' => "West Virginia, US",
        'WI' => "Wisconsin, US",
        'WY' => "Wyoming, US",
        'AB' => "Alberta, CA",
        'BC' => "British Columbia, CA",
        'MB' => "Manitoba, CA",
        'NB' => "New Brunswick, CA",
        'NL' => "Newfoundland and Labrador, CA",
        'NT' => "Northwest Territories, CA",
        'NS' => "Nova Scotia, CA",
        'NU' => "Nunavut, CA",
        'ON' => "Ontario, CA",
        'PE' => "Prince Edward Island, CA",
        'QC' => "Quebec, CA",
        'SK' => "Saskatchewan, CA",
        'YT' => "Yukon, CA"
    );

    public static $asmeRequirements = array(
        '0' => 'No',
        '1' => 'Yes'
    );

    public static $mountingOptions = array(
        'Wall'   => 'Wall Mount Only',
        'Floor'  => 'Floor Mount Only',
        'Either' => 'Wall or Floor Mount'
    );

    public static $fuelSupplies = array(
        'Natural' => 'Natural Gas',
        'Propane' => 'Propane'
    );

    public function __construct($api = false, $isPreview = false)
    {
        $this->api       = $api;
        $this->isPreview = $isPreview;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('business', 'entity', array(
                'required'      => true,
                'label'         => 'Business Type',
                'class'         => 'IntellihotAppBundle:Business',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                'empty_value'   => 'Choose a business type'
            ))
            ->add('asmeRequirement', 'choice', array(
                'required'    => true,
                'label'       => 'ASME Requirement',
                'choices'     => self::$asmeRequirements,
                'data'        => '1',
                'empty_value' => false,
                'multiple'    => false,
                'expanded'    => true
            ))
            ->add('mountingOption', 'choice', array(
                'required' => true,
                'label'    => 'Mounting Option',
                'choices'  => self::$mountingOptions,
                'data'     => 'Floor',
                'multiple' => false,
                'expanded' => true
            ))
            ->add('fuelSupply', 'choice', array(
                'required'    => true,
                'label'       => 'Fuel Supply',
                'choices'     => self::$fuelSupplies,
                'data'        => 'Natural',
                'empty_value' => false,
                'multiple'    => false,
                'expanded'    => true
            ))
            ->add('altitude', 'integer', array(
                'required' => false,
                'label'    => 'if greater than 2000 ft',
                'attr'     => array('min' => '0')
            ))
            ->add('incomingWaterTemperature', 'integer', array(
                'required' => true,
                'label'    => 'Incoming (°F)',
                'attr'     => array('min' => '35'),
                'data'     => '40'
            ))
            ->add('desiredOutputTemperature', 'integer', array(
                'required' => true,
                'label'    => 'Fixture Temp  (°F)',
                'attr'     => array('placeholder' => '105 °F', 'min' => '1', 'max' => '180'),
                'data'     => '105'
            ))
            ->add('rooms', 'hidden');

        if ($this->isPreview == false) {
            $builder
                ->add('name', 'text', array(
                    'required' => true,
                    'label'    => 'Your Name'
                ))
                ->add('engineer', 'text', array(
                    'required' => false,
                    'label'    => 'Specifying Engineer'
                ))
                ->add('contractor', 'text', array(
                    'required' => false,
                    'label'    => 'Contractor'
                ))
                ->add('email', 'email', array(
                    'required' => true,
                    'label'    => 'Your Email'
                ))
                ->add('phone', 'text', array(
                    'required' => true,
                    'label'    => 'Your Phone'
                ))
                ->add('city', 'text', array(
                    'required' => true,
                    'label'    => 'Project City'
                ))
                ->add('state', 'choice', array(
                    'required'    => true,
                    'label'       => 'Project State/Province',
                    'choices'     => self::$states,
                    'empty_value' => 'Choose a state/province'
                ))
                ->add('zipcode', 'text', array(
                    'label'    => 'Your Zip/Postal Code',
                    'required' => true
                ))
                ->add('projectName', 'text', array(
                    'required' => true,
                    'label'    => 'Project Name'
                ))
                ->add('projectNumber', 'text', array(
                    'required' => false,
                    'label'    => 'Project Number'
                ))
                ->add('projectDescription', 'textarea', array(
                    'required' => true,
                    'label'    => 'Project Description'
                ))
                ->add('additionalComments', 'textarea', array(
                    'required' => false,
                    'label'    => 'Additional Comments'
                ))
                ->add('pointTemperature', 'integer', array(
                    'required' => true,
                    'label'    => 'Unit Setpoint  (°F)',
                    'attr'     => array('placeholder' => '120 °F', 'min' => '1', 'max' => '180'),
                    'data'     => '120'
                ));
        }

        if ($this->api) {
            $builder->add('fixtures', 'hidden', array(
                'mapped' => false
            ));
        } else {
            $builder
                ->add('fixtureUnits', 'hidden')
                ->add('fixtureDetails', 'hidden')
                ->add('altitudeFactor', 'hidden')
                ->add('gpm', 'hidden')
                ->add('btu', 'hidden')
                ->add('selections', 'hidden');
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'Intellihot\AppBundle\Entity\Submission',
            'validation_groups' => array('full_step', 'Default')
        ));

        if ($this->api) {
            if ($this->isPreview == false) {
                $resolver->setDefaults(array(
                    'data_class'        => 'Intellihot\AppBundle\Entity\Submission',
                    'csrf_protection'   => false,
                    'validation_groups' => array('full_step', 'Default')
                ));
            } else {
                $resolver->setDefaults(array(
                    'data_class'        => 'Intellihot\AppBundle\Entity\Submission',
                    'csrf_protection'   => false,
                    'validation_groups' => array('Default')
                ));
            }
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        if ($this->api) {
            return '';
        } else {
            return 'intellihot_appbundle_submission';
        }
    }
}
