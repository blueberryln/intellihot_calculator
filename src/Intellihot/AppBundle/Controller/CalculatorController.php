<?php

namespace Intellihot\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Intellihot\AppBundle\Entity\Submission;
use Intellihot\AppBundle\Form\SubmissionType;

class CalculatorController extends Controller
{
    /**
     * Index
     *
     * @Route("/", name="index", options={"expose"=true})
     * @Template()
     */
    public function indexAction(Request $request)
    {
        // Get fixture and business repos
        $em           = $this->getDoctrine()->getManager();
        $fixtureRepo  = $em->getRepository('IntellihotAppBundle:Fixture');
        $fixtures     = $fixtureRepo->findBy(array(), array('name' => 'ASC'));
        $businessRepo = $em->getRepository('IntellihotAppBundle:Business');
        $businesses   = $businessRepo->findBy(array(), array('name' => 'ASC'));

        // Create form
        $submission = new Submission();
        $form       = $this->createForm(new SubmissionType(), $submission);

        // Handle form
        $form->handleRequest($request);

        // Render form
        return array(
            'form'       => $form->createView(),
            'businesses' => $businesses,
            'fixtures'   => $fixtures,
            'step'       => '1'
        );
    }

    /**
     * Calculate
     *
     * @Route("/calculate", name="calculate", options={"expose"=true})
     */
    public function calculateAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $data = $this->get('intellihot_app.calculator')->calculate(
                $request->request->get('business'),
                $request->request->get('asme'),
                $request->request->get('mount'),
                $request->request->get('altitude'),
                $request->request->get('waterTempIn'),
                $request->request->get('waterTempOut'),
                $request->request->get('fixtureUnits'),
                $request->request->get('debug'),
                $request->request->get('debugBtu')
            );

            return new JsonResponse($data);
        } else {
            return new Response('Sorry, this page cannot be accessed directly.');
        }
    }

    /**
     * Result
     *
     * @Route("/result", name="result", options={"expose"=true})
     * @Template()
     */
    public function resultAction(Request $request)
    {
        // Create form
        $submission = new Submission();
        $form       = $this->createForm(new SubmissionType(), $submission);

        // Handle form
        $form->handleRequest($request);

        // Get debugging state
        $debug = $this->get('request')->request->get('debug');

        // Get fixture amount
        $fixtures = array();
        foreach ($request->request->all() as $f => $amount) {
            if (strpos($f, 'fixture-') === 0 && $amount > 0) {
                $exploded               = explode('-', $f);
                $fixtures[$exploded[1]] = $amount;
            }
        }

        $request->getSession()->set('submission_fixtures', $fixtures);

        // Get form details
        $projectDetails = array(
            'businessId'      => $form->get('business')->getData()->getId(),
            'businessName'    => $form->get('business')->getData(),
            'rooms'           => $form->get('rooms')->getData(),
            'fixtureDetails'  => $form->get('fixtureDetails')->getData(),
            'asmeRequirement' => $form->get('asmeRequirement')->getData(),
            'mountingOption'  => $form->get('mountingOption')->getData(),
            'fuelSupply'      => $form->get('fuelSupply')->getData(),
            'altitude'        => $form->get('altitude')->getData(),
            'waterTempIn'     => $form->get('incomingWaterTemperature')->getData(),
            'waterTempOut'    => $form->get('desiredOutputTemperature')->getData(),
            'fixtureUnits'    => $form->get('fixtureUnits')->getData(),
            'altitudeFactor'  => $form->get('altitudeFactor')->getData(),
            'gpm'             => $form->get('gpm')->getData(),
            'btu'             => $form->get('btu')->getData(),
            'selections'      => $form->get('selections')->getData()
        );

        // Get selection details
        $selectionDetails = $this->get('intellihot_app.calculator')
            ->getSelectionDetails($projectDetails, trim($form->get('selections')->getData()));

        // Render form
        return array(
            'form'       => $form->createView(),
            'step'       => '2',
            'project'    => $projectDetails,
            'selections' => $selectionDetails,
            'debug'      => $debug
        );
    }

    /**
     * Submit
     *
     * @Route("/submit", name="submit", options={"expose"=true})
     * @Template()
     */
    public function submitAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            // Create form
            $submission = new Submission();
            $form       = $this->createForm(new SubmissionType(), $submission);

            // Handle form
            $form->handleRequest($request);

            // Process form
            if ($form->isValid()) {
                // Get form details
                $projectDetails = array(
                    'name'               => $form->get('name')->getData(),
                    'email'              => $form->get('email')->getData(),
                    'phone'              => $form->get('phone')->getData(),
                    'zipcode'            => $form->get('zipcode')->getData(),
                    'projectName'        => $form->get('projectName')->getData(),
                    'projectNumber'      => $form->get('projectNumber')->getData(),
                    'projectDescription' => $form->get('projectDescription')->getData(),
                    'additionalComments' => $form->get('additionalComments')->getData(),
                    'businessId'         => $form->get('business')->getData()->getId(),
                    'businessName'       => $form->get('business')->getData(),
                    'rooms'              => $form->get('rooms')->getData(),
                    'fixtureDetails'     => $form->get('fixtureDetails')->getData(),
                    'asmeRequirement'    => $form->get('asmeRequirement')->getData(),
                    'mountingOption'     => $form->get('mountingOption')->getData(),
                    'fuelSupply'         => $form->get('fuelSupply')->getData(),
                    'altitude'           => $form->get('altitude')->getData(),
                    'waterTempIn'        => $form->get('incomingWaterTemperature')->getData(),
                    'waterTempOut'       => $form->get('desiredOutputTemperature')->getData(),
                    'altitudeFactor'     => $form->get('altitudeFactor')->getData(),
                    'fixtureUnits'       => $form->get('fixtureUnits')->getData(),
                    'gpm'                => $form->get('gpm')->getData(),
                    'btu'                => $form->get('btu')->getData(),
                    'selections'         => $form->get('selections')->getData(),
                    'engineer'           => $form->get('engineer')->getData(),
                    'contractor'         => $form->get('contractor')->getData()
                );

                // Assign Rsm and Representatives to submission
                $this->get('intellihot_app.calculator')->assignRsmAndRepresentativeToSubmission($submission);

                // Get selection details
                $selectionDetails = $this->get('intellihot_app.calculator')
                    ->getSelectionDetails($projectDetails, trim($form->get('selections')->getData()));

                // Generate pdfs
                $schematicsDetails = $this->get('intellihot_app.calculator')
                    ->generatePdfs($submission, $request->getSession()->get('submission_fixtures'), $selectionDetails);

                // Save schematics to session
                $request->getSession()->set('schematics', $schematicsDetails);

                // Redirect form
                return $this->redirectToRoute('submit');
            } else {
                // Invalid submission
                return array(
                    'success' => false
                );
            }
        } else {
            // Valid submission
            $schematicsDetails = $request->getSession()->get('schematics');

            if ($schematicsDetails == null) {
                return $this->redirectToRoute('index');
            }

            return array(
                'success'    => true,
                'schematics' => $schematicsDetails
            );
        }
    }
}
