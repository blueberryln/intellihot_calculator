/**
 * Application scripts
 */

(function ($) {

    var isProcessing = false;

    /**
     * Initialize webshims
     */
    webshim.setOptions('forms', {
        replaceValidationUI: true
    });

    $.webshims.setOptions('basePath', '/bundles/intellihotapp/js/lib/shims/');
    $.webshims.polyfill('forms validation picture');

    /**
     * Form control
     */
    // Show appropriate fixtures on page load
    var businessId = $('#intellihot_appbundle_submission_business').val();
    showFixtures(businessId);

    // Show appropriate fixtures on business type change
    $('#intellihot_appbundle_submission_business').change(function (e) {
        var businessId = $(this).val();
        showFixtures(businessId);
    });


    // Submit form first step
    $('#submit-step1').click(function (e) {
        e.preventDefault();
        isValid = $('#intellihot_appbundle_submission').callProp('checkValidity');

        if (isValid == true) {
            calculateFixtures();
        }
    });

    // Automatically populate appropriate fixtures based on room value
    $('.rooms').keyup(function () {
        addBathrooms();
    });

    $('.rooms').change(function () {
        addBathrooms();
    });

    // Assign back button actions
    $('.btn-back').click(function (e) {
        e.preventDefault();
        window.history.back();
    });

    // Assign fixture details show/hide actions
    $("#show-fixture-details").click(function () {
        if ($('#fixture-details:visible').length) {
            $('#fixture-details').fadeOut();
            $('#show-fixture-details').html('(Show Details)')
        } else {
            $('#fixture-details').fadeIn();
            $('#show-fixture-details').html('(Hide Details)')
        }
    });

    // Fix fixture column heights on page load and window resize
    var columns = $('.fixture');
    $(window).resize(function () {
        equalizeHeight(columns, 767);
    });
    equalizeHeight(columns, 767);

    /**
     * Form control functions
     */
    // Function to show appropriate fixtures for a business type
    function showFixtures(businessId) {
        if (businessId) {
            $('.fixtures').hide();
            $('.fixtures-' + businessId).show();
            $('.fixtures').removeClass('active-fixtures');
            $('.fixtures-' + businessId).addClass('active-fixtures');
            $('.form-fixtures').fadeIn();

            // Fix column heights
            var columns = $('.fixture');
            equalizeHeight(columns, 767);
        }
    }

    // Function to automatically populate certain input values based on room quantity
    function addBathrooms() {
        var rooms = $('.active-fixtures').find('#rooms').val();

        $('.active-fixtures').find('input[data-auto-populate=1]').val(rooms);
    }

    /**
     * Calculation functions
     */

    // Function to calculate fixture units
    function calculateFixtures() {
        // Get debug variables from query string
        var debug = getParameterByName('debug');
        var debugNoSubmit = getParameterByName('nosubmit');
        var debugBtu = getParameterByName('btu');

        if (debug) {
            $('#intellihot_appbundle_submission').append('<input type="hidden" id="debug" name="debug" value="1"/>');
        }

        // Get form values
        var rooms = $('.active-fixtures').find('#rooms').val();
        var business = $('#intellihot_appbundle_submission_business').val();
        var asme = $('input[type=radio][name="intellihot_appbundle_submission[asmeRequirement]"]:checked').val();
        var mount = $('input[type=radio][name="intellihot_appbundle_submission[mountingOption]"]:checked').val();
        var altitude = $('#intellihot_appbundle_submission_altitude').val();
        var waterTempIn = $('#intellihot_appbundle_submission_incomingWaterTemperature').val();
        var waterTempOut = $('#intellihot_appbundle_submission_desiredOutputTemperature').val();
        var fixtures = $('.active-fixtures').find('.fixture input');

        var fixtureUnits = 0;
        var fixtureDetails = "";

        // Calculate fixture units
        $.each(fixtures, function () {
            // Calculate fixture values
            fixtureVal = $(this).val();
            fixtureName = $(this).attr('data-name');
            fixtureWeight = $(this).attr('data-weight');
            fixtureCalc = fixtureVal * fixtureWeight;
            fixtureUnits += fixtureCalc;

            // Build fixture details
            if (fixtureVal) {
                fixtureDetails += " " + fixtureName + ": " + fixtureVal + " * " + fixtureWeight + " = " + fixtureCalc + " fu \n";
            }
        });

        // Add results to form
        $('#intellihot_appbundle_submission_rooms').val(rooms);
        $('#intellihot_appbundle_submission_fixtureUnits').val(fixtureUnits);
        $('#intellihot_appbundle_submission_fixtureDetails').val(fixtureDetails);

        /**
         * Get gpm, btu and unit selections
         */
        var jsonData = {
            asme: asme,
            mount: mount,
            altitude: altitude,
            business: business,
            waterTempIn: waterTempIn,
            waterTempOut: waterTempOut,
            fixtureUnits: fixtureUnits,
            debug: debug,
            debugBtu: debugBtu
        };

        if (isProcessing == false) {
            $.ajax({
                url: Routing.generate('calculate'),
                dataType: 'JSON',
                type: 'POST',
                data: jsonData,
                success: function (result) {

                    if (result.success == true) {

                        // Hide error messages
                        $('.flash-msg').hide();

                        // Get calculated altitude factor, gpm and btu
                        var altitudeFactor = result.altitudeFactor;
                        var gpm = result.gpm;
                        var btu = result.btu;
                        var selections = result.selections;

                        // Add values to form inputs
                        $('#intellihot_appbundle_submission_altitudeFactor').val(altitudeFactor);
                        $('#intellihot_appbundle_submission_gpm').val(gpm);
                        $('#intellihot_appbundle_submission_btu').val(btu);
                        $('#intellihot_appbundle_submission_selections').val(selections);

                        if (debug) {
                            log("Business Type: " + business);
                            log("Asme: " + asme);
                            log("Mount: " + mount);
                            log("Altitude: " + altitude);
                            log("\nFixture Details:\n" + fixtureDetails);
                            log("\nFixture Units: " + fixtureUnits);
                            log("Altitude Factor: " + altitudeFactor);
                            log("Gpm: " + gpm);
                            log("Btu: " + btu);
                            log("Selections: " + selections + "\n\n");
                        }

                        // Submit form
                        if (debugNoSubmit == true) {
                            window.showFlashMsg('Debugging logged to the console', 'warning', 0);
                        } else {
                            $("#intellihot_appbundle_submission").submit();
                        }

                    } else {
                        window.showFlashMsg(result.msg, 'danger', 0);
                    }
                }
            });
        }
    }

    /**
     * Utilities
     */

    // Safe logging
    function log(msg) {
        if (this.console) {
            console.log(msg);
        }
    }

    // Equalize element heights
    function equalizeHeight(elements, minWidth) {
        var elements = $(elements);
        var elementHeights = new Array();
        var windowWidth = $(window).width();

        // Reset element heights
        elements.height('auto');

        // Get tallest element height
        $.each(elements, function (i, element) {
            var elementHeight = $(element).height();
            elementHeights.push(elementHeight);
        });

        var tallestElement = Math.max.apply(null, elementHeights);

        // Equalize element heights
        if ((!minWidth ) || (windowWidth >= minWidth)) {
            elements.height(tallestElement);
        }
    }

    // Get URL parameter by name
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    // Global function to show flash messages
    window.showFlashMsg = function (msg, type, timeout) {

        //$("html, body").animate({ scrollTop: 0 }, 'fast');
        $('html,body').scrollTop(0);

        var flash = $('.flash-msg');

        flash.hide();
        flash.html(msg);
        flash.removeClass('bg-danger');
        flash.removeClass('bg-success');
        flash.removeClass('bg-warning');
        flash.addClass('bg-' + type);
        flash.slideDown('fast');

        if (timeout) {
            setTimeout(function () {
                flash.fadeOut();
            }, timeout);
        }
    };

})(jQuery);