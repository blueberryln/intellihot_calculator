<?php

namespace Intellihot\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class GpmAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('fixtureMin')
            ->add('fixtureMax')
            ->add('gpm0')
            ->add('gpm1')
            ->add('gpm2')
            ->add('gpm3')
            ->add('gpm4')
            ->add('gpm5')
            ->add('gpm6')
            ->add('gpm7')
            ->add('gpm8')
            ->add('gpm9')
            ->add('gpm10')
            ->add('gpm11')
            ->add('gpm12')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('fixtureMin')
            ->add('fixtureMax')
            ->add('gpm0')
            ->add('gpm1')
            ->add('gpm2')
            ->add('gpm3')
            ->add('gpm4')
            ->add('gpm5')
            ->add('gpm6')
            ->add('gpm7')
            ->add('gpm8')
            ->add('gpm9')
            ->add('gpm10')
            ->add('gpm11')
            ->add('gpm12')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('fixtureMin')
            ->add('fixtureMax')
            ->add('gpm0')
            ->add('gpm1')
            ->add('gpm2')
            ->add('gpm3')
            ->add('gpm4')
            ->add('gpm5')
            ->add('gpm6')
            ->add('gpm7')
            ->add('gpm8')
            ->add('gpm9')
            ->add('gpm10')
            ->add('gpm11')
            ->add('gpm12')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('fixtureMin')
            ->add('fixtureMax')
            ->add('gpm0')
            ->add('gpm1')
            ->add('gpm2')
            ->add('gpm3')
            ->add('gpm4')
            ->add('gpm5')
            ->add('gpm6')
            ->add('gpm7')
            ->add('gpm8')
            ->add('gpm9')
            ->add('gpm10')
            ->add('gpm11')
            ->add('gpm12')
        ;
    }
}
