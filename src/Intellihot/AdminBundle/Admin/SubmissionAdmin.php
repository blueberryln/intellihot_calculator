<?php

namespace Intellihot\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Intellihot\AppBundle\Form\SubmissionType;

class SubmissionAdmin extends Admin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by'    => 'id'
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('business')
            ->add('id')
            ->add('name')
            ->add('email')
            ->add('phone')
            ->add('zipcode')
            ->add('city')
            ->add('state')
            ->add('projectName')
            ->add('projectNumber')
            ->add('projectDescription')
            ->add('additionalComments')
            ->add('asmeRequirement')
            ->add('mountingOption')
            ->add('fuelSupply')
            ->add('altitude')
            ->add('incomingWaterTemperature')
            ->add('desiredOutputTemperature')
            ->add('rooms')
            ->add('fixtureDetails', null, array('label' => 'Fixture Details'))
            ->add('fixtureUnits', null, array('label' => 'Total Fixture Units'))
            ->add('altitudeFactor', null, array('label' => 'Altitude Factor'))
            ->add('gpm', null, array('label' => 'Calculated GPM'))
            ->add('btu', null, array('label' => 'Calculated BTU'))
            ->add('selections')
            ->add('schematics')
            ->add('engineer')
            ->add('contractor')
            ->add('created');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('projectName', null, array('label' => 'Project'))
            ->add('projectNumber', null, array('label' => 'Nº'))
            ->add('business.name', null, array('label' => 'Business'))
            ->add('asmeRequirement', null, array('label' => 'Asme'))
            ->add('mountingOption', null, array('label' => 'Mount'))
            ->add('fuelSupply', null, array('label' => 'Fuel'))
            ->add('altitude', null, array('label' => 'Alt'))
            ->add('incomingWaterTemperature', null, array('label' => 'IWT'))
            ->add('desiredOutputTemperature', null, array('label' => 'DOT'))
            ->add('rooms')
            ->add('fixtureUnits', null, array('label' => 'FU'))
            ->add('altitudeFactor', null, array('label' => 'AF'))
            ->add('gpm', null, array('label' => 'GPM'))
            ->add('btu', 'number', array('label' => 'BTU', 'precision' => 2))
            ->add('selections')
            ->add('created')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show'   => array(),
                    'edit'   => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('email')
            ->add('phone')
            ->add('zipcode')
            ->add('city')
            ->add('state', 'choice', array('choices' => SubmissionType::$states))
            ->add('projectName')
            ->add('projectNumber')
            ->add('projectDescription')
            ->add('additionalComments')
            ->add('business')
            ->add('asmeRequirement')
            ->add('mountingOption')
            ->add('fuelSupply')
            ->add('altitude')
            ->add('incomingWaterTemperature')
            ->add('desiredOutputTemperature')
            ->add('rooms')
            ->add('fixtureDetails', null, array('label' => 'Fixture Details'))
            ->add('fixtureUnits', null, array('label' => 'Total Fixture Units'))
            ->add('altitudeFactor', null, array('label' => 'Altitude Factor'))
            ->add('gpm', null, array('label' => 'Calculated GPM'))
            ->add('btu', null, array('label' => 'Calculated BTU'))
            ->add('selections')
            ->add('engineer')
            ->add('contractor');
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('email')
            ->add('phone')
            ->add('zipcode')
            ->add('city')
            ->add('state')
            ->add('projectName')
            ->add('projectNumber')
            ->add('projectDescription')
            ->add('additionalComments')
            ->add('asmeRequirement')
            ->add('business')
            ->add('mountingOption')
            ->add('fuelSupply')
            ->add('altitude')
            ->add('incomingWaterTemperature')
            ->add('desiredOutputTemperature')
            ->add('rooms')
            ->add('fixtureDetails', null, array('label' => 'Fixture Details'))
            ->add('fixtureUnits', null, array('label' => 'Total Fixture Units'))
            ->add('altitudeFactor', null, array('label' => 'Altitude Factor'))
            ->add('gpm', null, array('label' => 'Calculated GPM'))
            ->add('btu', null, array('label' => 'Calculated BTU'))
            ->add('selections')
            ->add('schematics')
            ->add('engineer')
            ->add('contractor')
            ->add('created');
    }

    public function getExportFields()
    {
        return array(
            'id', 'name', 'email', 'phone', 'zipcode', 'city', 'state', 'projectName', 'projectNumber',
            'projectDescription', 'additionalComments', 'asmeRequirement', 'business', 'mountingOption', 'fuelSupply',
            'altitude', 'incomingWaterTemperature', 'desiredOutputTemperature', 'rooms', 'fixtureDetails', 'fixtureUnits',
            'altitudeFactor', 'gpm', 'btu', 'selections', 'schematics', 'engineer', 'contractor', 'created'
        );
    }
}
