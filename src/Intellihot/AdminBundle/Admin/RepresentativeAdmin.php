<?php

namespace Intellihot\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Intellihot\AppBundle\Form\SubmissionType;

class RepresentativeAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('zipcode')
            ->add('company')
            ->add('email')
            ->add('phone')
            ->add('primaryCity')
            ->add('areaCode')
            ->add('county')
            ->add('state', null, array(), 'choice', array(
                'choices' => SubmissionType::$states
            ));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('zipcode')
            ->add('company')
            ->add('email')
            ->add('phone')
            ->add('primaryCity')
            ->add('areaCode')
            ->add('county')
            ->add('state', 'choice', array('choices' => SubmissionType::$states))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show'   => array(),
                    'edit'   => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('zipcode')
            ->add('company', null, array('required' => false))
            ->add('email', 'email', array('required' => false))
            ->add('phone', null, array('required' => false))
            ->add('primaryCity', null, array('required' => false))
            ->add('areaCode', null, array('required' => false))
            ->add('county', null, array('required' => false))
            ->add('state', 'choice', array(
                'choices'  => SubmissionType::$states,
                'required' => false
            ));
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('zipcode')
            ->add('company')
            ->add('email')
            ->add('phone')
            ->add('primaryCity')
            ->add('areaCode')
            ->add('county')
            ->add('state', 'choice', array('choices' => SubmissionType::$states));
    }
}
