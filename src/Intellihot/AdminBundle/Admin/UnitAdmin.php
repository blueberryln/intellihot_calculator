<?php

namespace Intellihot\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UnitAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('quantity')
            ->add('btu')
            ->add('image')
            ->add('schematic');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('quantity')
            ->add('btu')
            ->add('image')
            ->add('schematic')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show'   => array(),
                    'edit'   => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('quantity')
            ->add('btu')
            ->add('image', 'choice', array('choices' => array('wall' => 'wall', 'floor' => 'floor', 'neuron' => 'neuron')));

        if ($this->getSubject()->getId() == null || strlen($this->getSubject()->getSchematic()) < 1) {
            $formMapper
                ->add('schematicFile', 'file', array(
                    'required' => true
                ));
        } else {
            $filePath = $this->getConfigurationPool()->getContainer()
                ->get('vich_uploader.templating.helper.uploader_helper')
                ->asset($this->getSubject(), 'schematicFile');

            $formMapper
                ->add('schematicFile', 'file', array(
                    'required' => false,
                    'help'     => "<a href='" . $filePath . "' target='_blank' class='btn btn-default' style='margin-top: 10px;'>View Schematic</a>",
                ));
        }
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('quantity')
            ->add('btu')
            ->add('image')
            ->add('schematic');
    }
}
