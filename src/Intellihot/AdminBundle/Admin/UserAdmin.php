<?php

namespace Intellihot\AdminBundle\Admin;

use Sonata\UserBundle\Admin\Entity\UserAdmin as BaseUserAdmin;

class UserAdmin extends BaseUserAdmin
{
    protected $baseRouteName = 'user_admin';
    protected $baseRoutePattern = 'user';
}