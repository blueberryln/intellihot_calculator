<?php

namespace Intellihot\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ClientAdmin extends Admin
{
    /**
     * Customize Datagrid values
     *
     * @var array
     */
    protected $datagridValues = array(
        '_page'       => 1,
        '_per_page'   => 25,
        '_sort_order' => 'ASC',
        '_sort_by'    => 'key'
    );

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection->add(
            'key_generate', 'key/generate',
            array('_controller' => 'IntellihotAdminBundle:Client:generateKey'),
            array('_method' => 'POST')
        );
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('key', 'text', array(
                'attr' => array(
                    'class' => 'span5 client-key'
                )
            ))
            ->setHelps(array(
                'key' => '<a href="javascript:void(0);" class="generate-client-key">Generate New Key</a>'
            ));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('key');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('key');
    }
}