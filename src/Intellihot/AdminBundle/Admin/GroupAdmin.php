<?php

namespace Intellihot\AdminBundle\Admin;

use Sonata\UserBundle\Admin\Entity\GroupAdmin as BaseGroupAdmin;

class GroupAdmin extends BaseGroupAdmin
{
    protected $baseRouteName = 'group_admin';
    protected $baseRoutePattern = 'group';
}