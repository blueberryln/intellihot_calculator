<?php

namespace Intellihot\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SubmittalAdmin extends Admin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show'   => array(),
                    'edit'   => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array(
                'read_only' => true
            ));

        if ($this->getSubject()->getId() == null || strlen($this->getSubject()->getSubmittal()) < 1) {
            $formMapper
                ->add('submittalFile', 'file', array(
                    'required' => true
                ));
        } else {
            $filePath = $this->getConfigurationPool()->getContainer()
                ->get('vich_uploader.templating.helper.uploader_helper')
                ->asset($this->getSubject(), 'submittalFile');

            $formMapper
                ->add('submittalFile', 'file', array(
                    'required' => false,
                    'help'     => "<strong>Filename must be matched with submittal name and file must be created with pdf version <= 1.4</strong><br/><a href='" . $filePath . "' target='_blank' class='btn btn-default' style='margin-top: 10px;'>View Submittal</a>",
                ));
        }
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name');
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        if ($object->getSubmittalFile() instanceof UploadedFile) {
            if ($object->getSubmittalFile()->getClientOriginalName() !== $object->getName()) {
                $errorElement
                    ->with('submittalFile')
                    ->addViolation('Filename is not matched: ' . $object->getName())
                    ->end();
            }
        }
    }
}
