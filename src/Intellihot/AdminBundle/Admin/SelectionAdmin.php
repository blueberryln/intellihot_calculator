<?php

namespace Intellihot\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SelectionAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('btuMin')
            ->add('btuMax')
            ->add('wallNonAsme')
            ->add('floorNonAsme')
            ->add('eitherNonAsme')
            ->add('wallAsme')
            ->add('floorAsme')
            ->add('eitherAsme')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('btuMin')
            ->add('btuMax')
            ->add('wallNonAsme')
            ->add('floorNonAsme')
            ->add('eitherNonAsme')
            ->add('wallAsme')
            ->add('floorAsme')
            ->add('eitherAsme')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('btuMin')
            ->add('btuMax')
            ->setHelps(array(
                'title' => 'Set the title of a web page',
                'keywords' => 'Set the keywords of a web page',
            ))
            ->add('wallNonAsme')
            ->add('floorNonAsme')
            ->add('eitherNonAsme')
            ->add('wallAsme')
            ->add('floorAsme')
            ->add('eitherAsme')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('btuMin')
            ->add('btuMax')
            ->add('wallNonAsme')
            ->add('floorNonAsme')
            ->add('eitherNonAsme')
            ->add('wallAsme')
            ->add('floorAsme')
            ->add('eitherAsme')
        ;
    }
}
