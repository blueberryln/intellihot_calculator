<?php

namespace Intellihot\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Intellihot\AppBundle\Entity\Option;

class OptionAdmin extends Admin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'label'
    );

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'read_only' => true,
                'required' => false
            ))
        ;

        if ($this->getSubject()) {
            if ($this->getSubject()->getName() == Option::MANAGER_EMAIL_ROUTING) {
                $formMapper->add('value', 'choice', array(
                    'choices' => array(
                        Option::MANAGER_EMAIL_ROUTING_ON => Option::MANAGER_EMAIL_ROUTING_ON,
                        Option::MANAGER_EMAIL_ROUTING_OFF => Option::MANAGER_EMAIL_ROUTING_OFF
                    )
                ));
            } else if ($this->getSubject()->getName() == Option::REPRESENTATIVE_EMAIL_ROUTING) {
                $formMapper->add('value', 'choice', array(
                    'choices' => array(
                        Option::REPRESENTATIVE_EMAIL_ROUTING_ON => Option::REPRESENTATIVE_EMAIL_ROUTING_ON,
                        Option::REPRESENTATIVE_EMAIL_ROUTING_OFF => Option::REPRESENTATIVE_EMAIL_ROUTING_OFF
                    )
                ));
            } else if ($this->getSubject()->getName() == Option::INTELLIHOT_EMAIL_ROUTING) {
                $formMapper->add('value', 'choice', array(
                    'choices' => array(
                        Option::INTELLIHOT_EMAIL_ROUTING_ON => Option::INTELLIHOT_EMAIL_ROUTING_ON,
                        Option::INTELLIHOT_EMAIL_ROUTING_OFF => Option::INTELLIHOT_EMAIL_ROUTING_OFF
                    )
                ));
            } else if ($this->getSubject()->getName() == Option::INTELLIHOT_EMAIL) {
                $formMapper->add('value', 'email');
            } else if ($this->getSubject()->getName() == Option::INTELLIHOT_EMAIL_NAME) {
                $formMapper->add('value', 'text');
            }
        }
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('label')
            ->add('value')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('label')
            ->add('value')
        ;
    }
}