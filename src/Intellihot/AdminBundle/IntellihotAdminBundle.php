<?php

namespace Intellihot\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IntellihotAdminBundle extends Bundle
{
    public function getParent()
    {
        return 'SonataUserBundle';
    }
}
