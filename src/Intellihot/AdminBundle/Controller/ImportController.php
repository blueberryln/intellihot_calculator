<?php

namespace Intellihot\AdminBundle\Controller;

use Intellihot\AppBundle\Entity\Representative;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Ddeboer\DataImport\Reader\ExcelReader;

/**
 * @Route("/admin/import")
 */
class ImportController extends Controller
{
    /**
     * Import Representative
     *
     * @Method({"POST"})
     * @Route("/representative", name="admin_import_representative")
     */
    public function importRepresentativeAction(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        $status           = true;
        $file             = $request->files->get('data');
        $deleteCurrentRep = $request->get('delete');

        if (null !== $file) {
            try {
                if ($deleteCurrentRep == 'on') {
                    // remove old representative
                    $this->getDoctrine()->getConnection()->exec(
                        'DELETE FROM submissions_representatives;
                        DELETE FROM representative;
                        ALTER TABLE representative AUTO_INCREMENT = 1;'
                    );
                }

                // get representative data

                if (($handle = fopen($file->getPathname(), 'r')) !== false) {
                    // skip first row (column title)
                    fgetcsv($handle);

                    $counter = 0;

                    while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                        $rep = array(
                            'zipcode'     => str_pad($data[0], 5, '0', STR_PAD_LEFT),
                            'primaryCity' => trim($data[1]),
                            'state'       => trim($data[2]),
                            'county'      => trim($data[3]),
                            'areaCode'    => trim($data[4]),
                            'company'     => trim($data[5]),
                            'email'       => trim($data[6]),
                            'phone'       => trim($data[7])
                        );

                        if (strlen($rep['zipcode']) > 1 && strlen($rep['email']) > 3 && strpos($rep['email'], '@') !== false) {
                            $counter++;

                            $newRep = new Representative();

                            foreach ($rep as $field => $value) {
                                $field = 'set' . ucfirst($field);

                                $newRep->$field($value);
                            }

                            $this->getDoctrine()->getManager()->persist($newRep);

                            if ($counter >= 1000) {
                                // flush the latest rows
                                $this->getDoctrine()->getManager()->flush();
                                $this->getDoctrine()->getManager()->clear();

                                $counter = 0;
                            }
                        }
                    }

                    // flush the latest rows
                    $this->getDoctrine()->getManager()->flush();
                    $this->getDoctrine()->getManager()->clear();

                    fclose($handle);
                }
            } catch (\Exception $e) {
                echo $e->getMessage();
                $status = false;
            }
        } else {
            $status = false;
        }

        return new JsonResponse(array(
            'status' => $status
        ));
    }
}