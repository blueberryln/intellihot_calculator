<?php

namespace Intellihot\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ClientController extends Controller
{
    public function generateKeyAction()
    {
        $key = hash('sha256', '-' . time() . '-' . rand(20000, 1000000000) . '-' . $this->container->getParameter('secret'));

        return new JsonResponse(array('key' => $key));
    }
}